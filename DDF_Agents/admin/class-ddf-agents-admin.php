<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://nanomedia.ca
 * @since      1.0.0
 *
 * @package    Ddf_Agents
 * @subpackage Ddf_Agents/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ddf_Agents
 * @subpackage Ddf_Agents/admin
 * @author     Tony Lee <var.tonylee@gmail.com>
 */
class Ddf_Agents_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ddf_Agents_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ddf_Agents_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ddf-agents-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ddf_Agents_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ddf_Agents_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ddf-agents-admin.js', array( 'jquery' ), $this->version, false );

	}
	public function register_ddf_agents() {

		$arg = array(
			'labels'        => array(
				'name'               => 'DDF Agents',
				'singular_name'      => 'DDF Agent',
				'add_new'            => 'Add New Agent',
				'add_new_item'       => 'Add New Agent',
				'edit_item'          => 'Edit Agent',
				'new_item'           => 'Add New Agent',
				'view_item'          => 'View Agent',
				'search_items'       => 'Search Agent',
				'not_found'          => 'No Agent Found',
				'not_found_in_trash' => 'No Agent Found in Trash'
			),
			'query_var'     => 'ddflistings',
			'rewrite'       => array(
				'slug' => 'ddf_agents',
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 101,
			'menu_icon'     => 'dashicons-admin-users',
			'supports'      => array(
				'title',
				'thumbnail'
			)

		);

		// Add Custom Post
		register_post_type( 'ddf_agents', $arg );
		$this->ddf_custom_post_columns( 'ddf_agents' );
	}

	public function ddf_custom_post_columns( $type_name = '' ) {

		$base_columns = array();
		switch ( $type_name ) {
			case 'ddf_agents':
				$base_columns = array(
					"title"        => "Agent Name",
					"Agent_Status" => "Status",
					"Agent_Key"    => "Agent Key",
					"Office_ID"    => "Office ID"
				);
				break;
			case 'ddf_manual_listing':
				$base_columns = array(
					"title"             => "Title/Address",
					"Expired_Date"      => "Expiry Date",
					"Property_Category" => "Property Category",
					"Property_Price"    => "Property Price"
				);
				break;
		}
		if(empty($base_columns)){
			die('No custom post column defined.');
			wp_die();
		}

		/**
		 * ADMIN COLUMN - Add Custom Post Column ( manage_edit-{$custom_post_type_name}_columns )
		 */
		add_filter( "manage_edit-{$type_name}_columns", function () use ( $base_columns ) {

			$base_columns['cb'] = "<input type='checkbox' />";
			return $base_columns;

		} );
		/**
		 * ADMIN COLUMN - CONTENT
		 */
		add_action( "manage_{$type_name}_posts_custom_column", function ( $column_name, $id ) {

			global $wpdb, $post, $taxonomy;

			switch ( $column_name ) {
				case 'Expired_Date':
					$data = get_post_meta( $post->ID, $column_name, true );
					echo ( $data > 0 ? date('Y-m-d', $data) : 0 );
					break;
				default:
					echo get_post_meta( $post->ID, $column_name, true );
					break;
			}

		}, 10, 2 );

		/*
		 * ADMIN COLUMN - SORTING - MAKE HEADERS SORTABLE
		 */
		add_filter( "manage_edit-{$type_name}_sortable_columns", function ( $columns ) use ( $base_columns ) {

			return wp_parse_args( $base_columns, $columns );

		} );
	}

	public function ddf_custom_meta_box_setting() {

		$ddf_agent = new_cmb2_box( array(
			'id'           => 'ddf_agents',
			'title'        => __( 'DDF Agent', 'cmb2' ),
			'object_types' => array( 'ddf_agents', ),
			'type'         => 'group',
			'fields'       => array(
				array(
					'name'        => 'Agent Status',
					'id'          => 'Agent_Status',
					'type'        => 'select',
					'default'     => 'activate',
					'options'     => array(
						'activate'   => __( 'Activate', 'cmb' ),
						'deactivate' => __( 'Deactivate', 'cmb' )
					)
				),
				array(
					'name'        => 'Agent Key',
					'id'          => 'Agent_Key',
					'type'        => 'text',
					'row_classes' => 'col-xs-6',
					'attributes'  => array(
						'required' => 'required',
					)
				),
				array(
					'name'        => 'Office ID',
					'id'          => 'Office_ID',
					'type'        => 'text',
					'row_classes' => 'col-xs-6'
				)
			)
		) );

	}
}
