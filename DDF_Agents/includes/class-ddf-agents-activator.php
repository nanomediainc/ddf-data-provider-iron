<?php

/**
 * Fired during plugin activation
 *
 * @link       http://nanomedia.ca
 * @since      1.0.0
 *
 * @package    Ddf_Agents
 * @subpackage Ddf_Agents/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ddf_Agents
 * @subpackage Ddf_Agents/includes
 * @author     Tony Lee <var.tonylee@gmail.com>
 */
class Ddf_Agents_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
