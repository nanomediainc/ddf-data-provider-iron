<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://nanomedia.ca
 * @since      1.0.0
 *
 * @package    Ddf_Agents
 * @subpackage Ddf_Agents/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ddf_Agents
 * @subpackage Ddf_Agents/includes
 * @author     Tony Lee <var.tonylee@gmail.com>
 */
class Ddf_Agents_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
