<?PHP
require("libs/MysqliDb.php");
require("libs/PHRets_CREA.php");

/* Script Variables */
// Lots of output, saves requests to a local file.
$debugMode = false;
// Initially, you should set this to something like "-2 years". Once you have all day, change this to "-48 hours" or so to pull incremental data
$TimeBackPull = "-15 days";

/* RETS Variables */

$RETS = new PHRets();
$RETSURL = "http://data.crea.ca/Login.svc/Login";
$RETSUsername = "sQ5oAmL0idpecN2MtWqDaTq0";
$RETSPassword = "dUxnQoPo7Z4KLTOa5iK0hRsT";
$RETS->Connect($RETSURL, $RETSUsername, $RETSPassword);
$RETS->AddHeader("RETS-Version", "RETS/1.7.2");
$RETS->AddHeader('Accept', '/');
$RETS->SetParam('compression_enabled', true);
$RETS_PhotoSize = "LargePhoto";
$RETS_LimitPerQuery = 1;
if($debugMode /* DEBUG OUTPUT */)
{
	//$RETS->SetParam("catch_last_response", true);
	$RETS->SetParam("debug_file", "/var/web/CREA_Anthony.txt");
	$RETS->SetParam("debug_mode", true);
}
/* NOTES
 * With CREA, You have to ask the RETS server for a list of IDs.
 * Once you have these IDs, you can query for 100 listings at a time
 * Example Procedure:
 * 1. Get IDs (500 Returned)
 * 2. Get Listing Data (1-100)
 * 3. Get Listing Data (101-200)
 * 4. (etc)
 * 0
 * 5. (etc)
 * 6. Get Listing Data (401-500)
 *
 * Each time you get Listing Data, you want to save this data and then download it's images...
 */

$listingID = $_GET['id'];

if(empty($listingID)) exit('Need an ID');

$results = $RETS->SearchQuery(
	"Property",
	"Property",
	"(ID={$listingID})",
	array( "Format" => "STANDARD-XML", "Count" => 1, "Culture"=>"en-CA", "Limit"=>1)
);

if( current( $results['Count'] ) < 1 ){
die('No Property Found.');
}

# Testing get listing
$property = current($results['Properties']);
echo '<pre>'.print_r( $property['OpenHouse']['Event'][0], true ).'</pre>';
echo '---------------';
echo '<pre>'.print_r( $property, true ).'</pre>';

# Testing get listing image
#echo 'Photo Size:'. count( $property['Photo']['PropertyPhoto']).'<br>';
#$photos = $RETS->GetObject("Property", 'LargePhoto', $listingID, '*');
#echo '<pre>'.print_r( $photos, true ).'</pre>';

$RETS->Disconnect();