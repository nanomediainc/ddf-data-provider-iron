<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    DDF_Direct_Listing
 * @subpackage DDF_Direct_Listing/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    DDF_Direct_Listing
 * @subpackage DDF_Direct_Listing/admin
 * @author     Tony Lee <var.tonylee@gmail.com>
 */
class DDF_Direct_Listing_Admin {

	/**
	 *  The public variable of this plugin
	 */

	public $options_display;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->options_display = get_option( 'ddf_display_options' );
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( $this->plugin_name.'-select2-css', dirname(plugin_dir_url( __FILE__ )) . '/vendor/select2-3.5.3/select2.css', array(), $this->version,'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ddf-direct-listing-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-jui', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-codemirror', dirname(plugin_dir_url( __FILE__ )) . '/vendor/codemirror/codemirror.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if( !isset($_GET['page']) or strpos($_GET['page'], 'ddf') === false ){
			return;
		}

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ddf-direct-listing-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-highcharts', dirname(plugin_dir_url( __FILE__ )) . '/vendor/highcharts/highcharts.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-exporting', dirname(plugin_dir_url( __FILE__ )) . '/vendor/highcharts/exporting.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-select2', dirname(plugin_dir_url( __FILE__ )) . '/vendor/select2-3.5.3/select2.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-codemirror', dirname(plugin_dir_url( __FILE__ )) . '/vendor/codemirror/codemirror.js', array(), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jqueryUI', dirname(plugin_dir_url( __FILE__ )) . '/vendor/jquery/jquery-ui.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-switchbtn', dirname(plugin_dir_url( __FILE__ )) . '/vendor/jquery/jquery.switchButton.js', array( 'jquery', $this->plugin_name.'-jqueryUI' ), $this->version, false );

	}

	/**
	 * Register custom post type into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function register_ddf_agents() {

		$arg = array(
			'labels'        => array(
				'name'               => 'DDF Agents',
				'singular_name'      => 'DDF Agent',
				'add_new'            => 'Add New Agent',
				'add_new_item'       => 'Add New Agent',
				'edit_item'          => 'Edit Agent',
				'new_item'           => 'Add New Agent',
				'view_item'          => 'View Agent',
				'search_items'       => 'Search Agent',
				'not_found'          => 'No Agent Found',
				'not_found_in_trash' => 'No Agent Found in Trash'
			),
			'query_var'     => 'ddflistings',
			'rewrite'       => array(
				'slug' => 'ddf_agents',
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 101,
			'menu_icon'     => 'dashicons-admin-users',
			'supports'      => array(
				'title',
				'thumbnail'
			)

		);

		// Add Custom Post
		register_post_type( 'ddf_agents', $arg );
		$this->ddf_custom_post_columns( 'ddf_agents' );
	}

	public function ddf_custom_post_columns( $type_name = '' ) {

		$base_columns = array();
		switch ( $type_name ) {
			case 'ddf_agents':
				$base_columns = array(
					"title"        => "Agent Name",
					"Agent_Status" => "Status",
					"Agent_Key"    => "Agent Key",
					"Office_ID"    => "Office ID"
				);
				break;
			case 'ddf_manual_listing':
				$base_columns = array(
					"title"             => "Title/Address",
					"Expired_Date"      => "Expiry Date",
					"Property_Category" => "Property Category",
					"Property_Price"    => "Property Price"
				);
				break;
		}
		if(empty($base_columns)){
			die('No custom post column defined.');
			wp_die();
		}

		/**
		 * ADMIN COLUMN - Add Custom Post Column ( manage_edit-{$custom_post_type_name}_columns )
		 */
		add_filter( "manage_edit-{$type_name}_columns", function () use ( $base_columns ) {

			$base_columns['cb'] = "<input type='checkbox' />";
			return $base_columns;

		} );
		/**
		 * ADMIN COLUMN - CONTENT
		 */
		add_action( "manage_{$type_name}_posts_custom_column", function ( $column_name, $id ) {

			global $wpdb, $post, $taxonomy;

			switch ( $column_name ) {
				case 'Expired_Date':
					$data = get_post_meta( $post->ID, $column_name, true );
					echo ( $data > 0 ? date('Y-m-d', $data) : 0 );
				    break;
				default:
					echo get_post_meta( $post->ID, $column_name, true );
					break;
			}

		}, 10, 2 );

		/*
		 * ADMIN COLUMN - SORTING - MAKE HEADERS SORTABLE
		 */
		add_filter( "manage_edit-{$type_name}_sortable_columns", function ( $columns ) use ( $base_columns ) {

			return wp_parse_args( $base_columns, $columns );

		} );
	}

	public function ddf_custom_meta_box_setting() {

		$ddf_agent = new_cmb2_box( array(
			'id'           => 'ddf_agents',
			'title'        => __( 'DDF Agent', 'cmb2' ),
			'object_types' => array( 'ddf_agents', ),
			'type'         => 'group',
			'fields'       => array(
				array(
					'name'        => 'Agent Status',
					'id'          => 'Agent_Status',
					'type'        => 'select',
					'default'     => 'activate',
					'options'     => array(
						'activate'   => __( 'Activate', 'cmb' ),
						'deactivate' => __( 'Deactivate', 'cmb' )
					)
				),
				array(
					'name'        => 'Agent Key',
					'id'          => 'Agent_Key',
					'type'        => 'text',
					'row_classes' => 'col-xs-6',
					'attributes'  => array(
						'required' => 'required',
					)
				),
				array(
					'name'        => 'Office ID',
					'id'          => 'Office_ID',
					'type'        => 'text',
					'row_classes' => 'col-xs-6'
				)
			)
		) );

		$ddf_manual = new_cmb2_box(array(
			'id'           => 'ddf_manual_listing',
			'title'        => __( 'Listing Information', 'cmb2' ),
			'object_types' => array( 'ddf_manual_listing', ),
			'type'         => 'group',
			'fields'       => array(
				array(
					'name'        => 'Property Category',
					'id'          => 'Property_Category',
					'type'        => 'select',
					'row_classes' => 'col-xs-6',
					'default'     => 'Exclusive',
					'options'     => array(
						'Exclusive'    => __( 'Exclusive Listing', 'cmb' ),
						'Past'    => __( 'Past Listing', 'cmb' )
					)
				),
				array(
					'name' => 'Expiry Date',
					'id'   => 'Expired_Date',
					'type' => 'text_date_timestamp',
					// 'timezone_meta_key' => 'wiki_test_timezone',
					'date_format' => 'Y-m-d',
					'row_classes' => 'col-xs-6',
				),
				array(
					'name' => 'Bedrooms',
					'id'   => 'Bedrooms',
					'type' => 'text',
					'row_classes' => 'col-xs-6',
				),
				array(
					'name' => 'Bathrooms',
					'id'   => 'Bathrooms',
					'type' => 'text',
					'row_classes' => 'col-xs-6',
				),
				array(
					'name'        => 'Property Price',
					'id'          => 'Property_Price',
					'type'        => 'text',
					'row_classes' => 'col-xs-6',
				),
				array(
					'name'        => 'Description',
					'id'          => 'Property_Desc',
					'type'        => 'textarea'
				),
				array(
					'name'        => 'Image Gallery',
					'desc'        => 'Upload property images.',
					'id'          => 'property_gallery',
					'type'        => 'file_list',
					// 'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
					// Optional, override default text strings
					'options'     => array(
						'add_upload_files_text' => 'Upload Images', // default: "Add or Upload Files"
					),
				),
			)
		));
	}
	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		$this->plugin_screen_hook_suffix = 'ddf';
		add_menu_page(
			__( 'DDF Direct Listing Settings', $this->plugin_name ),
			__( 'DDF Direct Listing', $this->plugin_name ),
			'administrator',
			$this->plugin_name,
			function(){
				echo '';
			},
			'dashicons-admin-home'
		);


		if(is_super_admin()) {

			add_submenu_page(
				$this->plugin_name,
				__( 'Shortcodes', $this->plugin_name ),
				__( 'Shortcodes', $this->plugin_name ),
				'manage_options',
				$this->plugin_name . '_shortcodes',
				function(){

					ob_start();
					include_once( 'partials/shortcodes.php' );
					$html = ob_get_clean();
					echo $html;
				});

			add_submenu_page(
				$this->plugin_name,
				__( 'DDF Direct Listing Cron Report', $this->plugin_name ),
				__( 'Sync Data Report', $this->plugin_name ),
				'manage_options',
				$this->plugin_name . '_report',
				function(){

					$param = array('action'=>'cron_report');
					$param['day'] = isset( $_REQUEST['cday'] ) ?  $_REQUEST['cday'] : '';
					$data = json_decode( post_data( $param ) );
					if($data) {
						$data = std_to_array( $data );

						$result = array();
						foreach ( $data as $k => $v ) {

							if ( ! isset( $result[ $v['sum_date'] ] ) ) {
								$result[ $v['sum_date'] ]['date'] = $v['sum_date'];
							}
							$result[ $v['sum_date'] ][ $v['sum_key'] ] = $v['sum_value'];

						}

						ob_start();
						include_once( 'partials/cron_report.php' );
						$html = ob_get_clean();
						echo $html;
					}else{
						echo '<div class="error"><h4>No internet connect</h4></div>';
					}

				});

			add_submenu_page(
				$this->plugin_name,
				__( 'DDF Direct Listing Admin Area', $this->plugin_name ),
				__( 'Admin Area', $this->plugin_name ),
				'manage_options',
				$this->plugin_name . '_admin',
				function(){
					ob_start();
					include_once( 'partials/admin.php' );
					$html = ob_get_clean();
					echo $html;
				}
			);

			add_submenu_page(
				$this->plugin_name,
				__( 'DDF Direct Listing Display Settings', $this->plugin_name ),
				__( 'Display Setting', $this->plugin_name ),
				'manage_options',
				$this->plugin_name . '_display',
				function(){
					ob_start();
					include_once( 'partials/display.php' );
					$html = ob_get_clean();
					echo $html;
				}
			);

			add_submenu_page(
				$this->plugin_name,
				__( 'DDF Direct Listing Custom CSS', $this->plugin_name ),
				__( 'Custom CSS', $this->plugin_name ),
				'manage_options',
				$this->plugin_name . '_css',
				function(){
					ob_start();
					include_once( 'partials/custom-css.php' );
					$html = ob_get_clean();
					echo $html;
				}
			);

			add_submenu_page(
				$this->plugin_name,
				__( 'DDF Direct Listing Custom JavaScript', $this->plugin_name ),
				__( 'Custom JavaScript', $this->plugin_name ),
				'manage_options',
				$this->plugin_name . '_js',
				function(){
					ob_start();
					include_once( 'partials/custom-js.php' );
					$html = ob_get_clean();
					echo $html;
				}
			);
		}
	}

	public function register_ddf_settings(){
		# Register Display Settings
		register_setting(
			'ddf_display_options',
			'ddf_display_options'
		);

		add_settings_section(
			'ddf_display_section',
			'Info',
			'',
			$this->plugin_name);

		add_settings_field  (
			'ddf_display_names',
			'Display Names',
			'',
			$this->plugin_name,
			'ddf_display_section');

		# Custom CSS and JS Setting
		register_setting( 'sccss_settings_group', 'sccss_settings' );
		register_setting( 'scjs_settings_group', 'scjs_settings' );
	}

	public function ddf_ajax_action(){
		if(!isset( $_POST['type'] )){
			wp_die();
		}
		if ( !isset($_POST['pid']) ) {
			wp_die();
		}

		switch(trim( $_POST['type'] )) {
			case 'search':


				$result = post_data( array( 'id' => trim( $_POST['pid'] ) ), 'checker' );
				break;

			case 'plus':
				$data = array(
					"tasks" => array(
						"0"=>array(
							"code_name"=>"W-Get-Listing-Photos-0",
							"payload"=> json_encode( array("listingID" => trim( $_POST['pid'] )) )
						)
					));

				$options = array("http" => array(
					"method" => "POST",
					"header" => array(
						"Accept-Encoding: gzip/deflate",
						"Authorization: OAuth Hc_bEGrJf2wpPJAzXkQkBKV3BBY",
						"Content-Type: application/json"
					),
					"content" => json_encode($data)
				)
				);
				$context = stream_context_create($options);
				$url = 'https://worker-aws-us-east-1.iron.io/2/projects/553ead7f09159d000600010c/tasks';
				$result = file_get_contents($url,false,$context);
				break;
		}

		echo json_encode($result);

		wp_die();
	}

	public function register_ddf_manual_listing(){

		$arg = array(
			'labels'        => array(
				'name'               => 'Manual Listing',
				'singular_name'      => 'Manual Listing',
				'add_new'            => 'Add New Listing',
				'add_new_item'       => 'Add New Listing',
				'edit_item'          => 'Edit Listing',
				'new_item'           => 'Add New Listing',
				'view_item'          => 'View Listing',
				'search_items'       => 'Search Listing',
				'not_found'          => 'No Listing Found',
				'not_found_in_trash' => 'No Listing Found in Trash'
			),
			'query_var'     => 'ddflistings',
			'rewrite'       => array(
				'slug' => 'ddf_manual_listing',
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 101,
			'menu_icon'     => 'dashicons-admin-home',
			'supports'      => array(
				'title',
				'thumbnail'
			)

		);

		// Add Custom Post
		register_post_type( 'ddf_manual_listing', $arg );
		$this->ddf_custom_post_columns( 'ddf_manual_listing' );

	}

	public function ddf_change_default_title($title){
		$screen = get_current_screen();

		if('ddf_manual_listing' == $screen->post_type){
			$title = 'Enter Title or Listing Address Here';
		}

		return $title;
	}
	public function ddf_remove_meta_boxes( $post_type ){

		if($post_type =='ddf_manual_listing') {

			remove_meta_box('eg-meta-box', $post_type, 'normal');
			remove_meta_box('wds_seomoz_urlmetrics', $post_type, 'normal');
			remove_meta_box('wds-wds-meta-box', $post_type, 'normal');
			remove_meta_box('mymetabox_revslider_0', $post_type, 'normal');
			remove_meta_box('copier-meta-box', $post_type, 'normal');

		}
	}
}