(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */


	$(function () {

		$(".select2").select2({
            placeholder:"Select Your Choice "
        });

        $(".agent_ids").on("change", function() {

            var data = $(this).select2('data');
            var ids = '';
            $.each(data, function( index, data ) {
                if( ids == '' ) {
                    ids = data.id;
                }else{
                    ids += ',' + data.id;
                }

            });
            $('pre.agent_ids_result').html('[ddf2 type="selected_agent_listings" meta="' + ids + '"]');
            $('pre.agent_ids_result_active').html('[ddf2 type="selected_agent_listings" meta="' + ids + '" listing_type="active"]');
            $('pre.agent_ids_result_past').html('[ddf2 type="selected_agent_listings" meta="' + ids + '" listing_type="past"]');

        });

        $(".office_ids").on("change", function() {

            var data = $(this).select2('data');
            var ids = '';
            $.each(data, function( index, data ) {
                if( ids == '' ) {
                    ids = data.id;
                }else{
                    ids += ',' + data.id;
                }

            });
            $('pre.office_ids_result').html('[ddf2 type="office_listings" meta="' + ids + '"]');

        });

        $(".l-display").on("change", function() {
            var data = $(this).select2('data');
            $('pre.l-display_result').html('[ddf2 type="city_listings" meta="' + data.id + '"]');
        });

        $(".cities").on("change", function() {
            var data = $(this).select2('data');
            $('pre.cities_result').html('[ddf2 type="city_listings" meta="' + data.id + '"]');
        });

        $(".city").on("change", function() {
            var data = $(this).select2('data');
            $('pre.city_meta_result').html('city="' + data.id + '"');
        });

        $('.postalcode').val('').on("keyup", function() {
            $('pre.postal_result').html('[ddf2 type="selected_postalcode_listings" meta="' + $('.postalcode').val() + '"]');
        });

        $('.property_id').val('').on("keyup", function() {
            $('pre.property_id_result').html('[ddf2 type="selected_property_id" meta="' + $('.property_id').val() + '"]');
        });

        $(".property_type").on("change", function() {
            var data = $(this).select2('data');
            var ids = '';
            $.each(data, function( index, data ) {
                if( ids == '' ) {
                    ids = data.id;
                }else{
                    ids += ',' + data.id;
                }

            });
            $('pre.property_type_result').html('[ddf2 type="selected_property_type" meta="' + ids + '"]');
        });

        $(".listing_type").on("change", function() {
            var data = $(this).select2('data');
            $('pre.listing_type_result').html('[ddf2 type="listing_type" meta="' + data.id + '"]');
        });
        $(".property_status").on("change", function() {
            var data = $(this).select2('data');
            $('pre.property_status_result').html('[ddf2 type="listing_type" meta="' + data.id + '"]');
        });

        $(".building_type").on("change", function() {
            var data = $(this).select2('data');
            var ids = '';
            $.each(data, function( index, data ) {
                if( ids == '' ) {
                    ids = data.id;
                }else{
                    ids += ',' + data.id;
                }

            });
            $('pre.building_type_result').html('[ddf2 type="selected_building_type" meta="' + ids + '"]');
        });

        $(".check-box").on('change', function(){
            var el = $(this).data('target');
            if( $(this).is(':checked')) {
                $('.'+el).html( $(this).data('on') );
            }else{
                $('.'+el).html( $(this).data('off') );
            }
        });

        $('.price_range').on('change', function(){

            var min=0, max= 0, type ='', meta_min='',  meta_max='', meta_type='';
            min = $('.min_price').select2('data');
            max = $('.max_price').select2('data');
            type = $('.sort_price').select2('data');

            console.log( type.id );

            if(min.id !='' && max.id !='' && max.id > 0  && parseInt(min.id) > parseInt(max.id)){
                alert("Min Price is over Max Price");
                return false;
            }
            if(!min.id && !max.id){
                alert('Must select a price limit.');
                return false;
            }
            if(min.id){
                meta_min = ' min="'+min.id+'"';
            }
            if(max.id){
                meta_max = ' max="'+max.id+'"';
            }
            if(type.id){
                meta_type= ' status="'+type.id+'"';
            }

            $('.price_range_result').html('[ddf2 type="selected_price"'+meta_min+meta_max+meta_type+']');
        });

        if( $('#container1').length > 0 ) {
			$('#container1').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Total Listings'
				},
				xAxis: {
					categories: info.date
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: false
					}
				},
				series: [
					{
						name: 'Total Listings',
						data: info.listings
					}
				]
			});

			$('#container2').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Last 10 Days Daily Cron Job Status'
				},
				xAxis: {
					categories: info.date
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: false
					}
				},
				series: [
					{
						name: 'Paid Agents',
						data: info.paid_agents
					},
					{
						name: 'Agent Listings',
						data: info.agent_listings
					},
					{
						name: 'Past Listings',
						data: info.agent_sold
					}
				]
			});
		}

		if (typeof tinymce != 'undefined' && $('.aggrement').length > 0) {
			tinymce.init({selector: 'textarea'});
		}


//-----------------------------------------------------------------
//Start: Silder
//-----------------------------------------------------------------
        var switch_btn_on = function(el){
            $(el).val(1);
        };
        var switch_btn_off = function(el){
            $(el).val(0);
        };
        $("#agent-option").switchButton({
            on_label: 'YES',
            off_label: 'NO',
            width: 60,
            height: 20,
            button_width: 30,
            callback_target:'#show_agent',
            on_callback: switch_btn_on,
            off_callback: switch_btn_off
        });

        $("#layout").switchButton({
            on_label: '<div class="dashicons dashicons-screenoptions"></div>',
            off_label: '<div class="dashicons dashicons-feedback"></div>',
            width: 60,
            height: 20,
            button_width: 30,
            callback_target:'#layout_option',
            on_callback: switch_btn_on,
            off_callback: switch_btn_off
        });

        // Slider 1
        var ini_day = $('#new_listing_days').val();
        $( "#slider" ).slider({
            value:ini_day,
            min: 1,
            max: 60,
            step: 1,
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.value );
                $('#new_listing_days').val( ui.value );
            }
        });
        $( "#amount" ).val( $( "#slider" ).slider( "value" ) );

        // Slider 2
        var ini_day = $('#listing_limit').val();
        $( "#limiter" ).slider({
            value:ini_day,
            min: 5,
            max: 200,
            step: 1,
            slide: function( event, ui ) {
                $( "#limit" ).val( ui.value );
                $('#listing_limit').val( ui.value );
            }
        });
        $( "#limit" ).val( $( "#limiter" ).slider( "value" ) );

//-----------------------------------------------------------------
//Display Setting Page
//-----------------------------------------------------------------
        $('.new-label').on('click', function(){
            var img = $(this).attr('class')+'-'+$(this).val(),
                src = $('img[role="'+img+'"]').attr('src');

            $('.certs').css('background-image', 'url("'+src+'")');
        });

        // onload
        var ini_src = $('img[role="new-label-'+$('.new-label:checked').val()+'"]').attr('src');
        $('.certs').css('background-image', 'url("'+ini_src+'")');


//-----------------------------------------------------------------
//Admin Setting
//-----------------------------------------------------------------

        $('.ddf-action').click(function(){

            var type = $(this).data('type');

            if(!$('#pid-'+type).val()){
                return '';
            }

            var that = $(this);

            $(this).html('checking...');
            $.ajax({
                type : "post",
                dataType : "json",
                url : ajaxurl,
                data : {
                    action  : "ddf_ajax",
                    type    : type,
                    pid     : $('#pid-'+type).val()
                },
                success: function(response) {
                    if(!response){
                        response = 'No Property Found.';
                    }
                    that.html('<span class="dashicons dashicons-'+type+'"></span>');
                    $('.result-'+type+' textarea').val( response );
                    $('.result-'+type).css('display', 'block');
                    console.log(type);
                    if(type == 'plus'){
                        var res = JSON.parse(response);
                        console.log(res);
                        var uri = 'https://hud.iron.io/tq/projects/553ead7f09159d000600010c/tasks/'+res['tasks'][0]['id']+'/log';
                        $('.job-log').attr('href', uri);
                    }
                }
            })

        });

	});

})( jQuery );
