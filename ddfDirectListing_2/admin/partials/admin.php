<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-09-25
 * Time: 5:29 PM
 */
?>
<div class="wrap">

	<h2>
		<span class="dashicons dashicons-admin-generic"></span>
		<?php echo esc_html( get_admin_page_title() ); ?>
	</h2>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Check Property</h3>
		</div>
		<div class="panel-body">
			<div class="input-group col-6">
				<input type="text" class="form-control" id="pid-search" placeholder="Enter Property ID">
                <span class="input-group-btn">
                    <button class="btn btn-default ddf-action" data-type="search" type="button">
	                    <span class="dashicons dashicons-search"></span>
                    </button>
                </span>
			</div>
			<div class="result result-search">
				<a href="http://www.jsoneditoronline.org/" target="_blank">view online</a>
				<br/>
				<textarea></textarea>
			</div>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Add Property Manually</h3>
		</div>
		<div class="panel-body">
			<div class="input-group col-6">
				<input type="text" class="form-control" id="pid-plus" placeholder="Enter Property ID">
                <span class="input-group-btn">
                    <button class="btn btn-default ddf-action" data-type="plus" type="button">
	                    <span class="dashicons dashicons-plus"></span>
                    </button>
                </span>
			</div>
			<div class="result result-plus">
				<a class="job-log" target="_blank">view worker</a>
				<br/>
				<textarea></textarea>
			</div>
		</div>
	</div>

</div>
