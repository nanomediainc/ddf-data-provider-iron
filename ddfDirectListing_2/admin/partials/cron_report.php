<?php
$info = array();
foreach($result as $total):
	$info['date'][]= $total['date'];
	$info['listings'][]= $total['listings'];
	$info['paid_agents'][]= $total['paid_agents'];
	$info['agent_listings'][]= $total['agent_listings'];
	$info['agent_sold'][]= $total['agent_sold'];
endforeach;

$reverse = array_reverse($result);
?>
<div id="container1"></div>
<div id="container2"></div>
<div class="wrap">
	<table class="wp-list-table plugins">
		<thead>
		<th class="header">Sync Date</th>
		<th class="header">Total Listings</th>
		<th class="header">DDF Agents</th>
		<th class="header">DDF Agents Active Listings</th>
		<th class="header">DDF Agents Past Listings</th>
		</thead>
		<tbody>
		<?php foreach( $reverse as $total): ?>
			<tr>
			<td class="option-content"><?=$total['date']?></td>
			<td class="option-content"><?=$total['listings']?></td>
			<td class="option-content"><?=$total['paid_agents']?></td>
			<td class="option-content"><?=$total['agent_listings']?></td>
			<td class="option-content"><?=$total['agent_sold']?></td>
			</tr>
		<?php endforeach;?>

		</tbody>
	</table>
</div>
<script>
	var info = {};
<?php
	echo "info.date=['".implode("','", $info['date'])."'];";
	echo "info.listings=[".implode(",", $info['listings'])."];";
	echo "info.paid_agents=[".implode(",", $info['paid_agents'])."];";
	echo "info.agent_listings=[".implode(",", $info['agent_listings'])."];";
	echo "info.agent_sold=[".implode(",", $info['agent_sold'])."];";
?>
	console.log( info );
</script>
