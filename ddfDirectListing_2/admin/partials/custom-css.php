<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   DDF_Direct_Listing_Admin
 * @author    Real Estate Apps<info@realestateapps.ca>
 * @license   GPL-2.0+
 * @link      http://www.realestateapps.ca
 * @copyright 2014 Real Estate Apps
 */

$options = get_option( 'sccss_settings' );
	$content = isset( $options['sccss-content'] ) && ! empty( $options['sccss-content'] ) ? $options['sccss-content'] : '/* Enter Your Custom CSS Here */';

	if ( isset( $_GET['settings-updated'] ) ) : 
?>
		<div id="message" class="updated"><p><?php _e( 'Custom CSS updated successfully.', 'sccss' ); ?></p></div>
	<?php endif; ?>
	<div class="wrap">

		<h2 style="margin-bottom: 1em;">
			<span class="dashicons dashicons-admin-generic"></span>
			<?php _e( 'DDF Custom CSS', 'sccss' ); ?>
		</h2>
		<form name="sccss-form" action="options.php" method="post" enctype="multipart/form-data">
			<div class="tips">
				<?php do_action( 'sccss-sidebar-top' ); ?>
				<p style="margin-top: 0"><?php _e( 'DDF Custom CSS allows you to add your own styles or override the default CSS of a plugin or theme.', 'sccss' ) ?></p>
				<p><?php _e( 'To use, enter your custom CSS, then click "Update Custom CSS".  It\'s that simple!', 'sccss' ) ?></p>
				<?php submit_button( __( 'Update Custom CSS', 'sccss' ), 'primary', 'submit', true ); ?>
				<?php do_action( 'sccss-sidebar-bottom' ); ?>
			</div>


			<?php settings_fields( 'sccss_settings_group' ); ?>

			<?php do_action( 'sccss-form-top' ); ?>

			<div>
				<textarea name="sccss_settings[sccss-content]" id="sccss_settings[sccss-content]" ><?php echo esc_html( $content ); ?></textarea>
			</div>

			<?php do_action( 'sccss-textarea-bottom' ); ?>

			<div>
				<?php submit_button( __( 'Update Custom CSS', 'sccss' ), 'primary', 'submit', true ); ?>
			</div>

			<?php do_action( 'sccss-form-bottom' ); ?>

		</form>
		<script language="javascript">
			jQuery( document ).ready( function() {
				var editor = CodeMirror.fromTextArea(
					document.getElementById( "sccss_settings[sccss-content]" ),
					{   lineNumbers: true,
						lineWrapping: true,
						matchBrackets: true,
						height: '900px'
					});
			});
		</script>
	</div>
<script src="<?=dirname(dirname(plugin_dir_url( __FILE__ ))) . '/vendor/codemirror/css.js'?>"></script>