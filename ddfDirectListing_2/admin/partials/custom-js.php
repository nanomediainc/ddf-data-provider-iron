<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   DDF_Direct_Listing_Admin
 * @author    Real Estate Apps<info@realestateapps.ca>
 * @license   GPL-2.0+
 * @link      http://www.realestateapps.ca
 * @copyright 2014 Real Estate Apps
 */

$options = get_option( 'scjs_settings' );
	$content = isset( $options['scjs-content'] ) && ! empty( $options['scjs-content'] ) ? $options['scjs-content'] : '/* Enter Your Custom JavaScript Here */';

	if ( isset( $_GET['scjs-updated'] ) ) :
?>
		<div id="message" class="updated"><p><?php _e( 'Custom JavaScript updated successfully.', 'scjs' ); ?></p></div>
	<?php endif; ?>
	<div class="wrap">
		<h2 style="margin-bottom: 1em;">
			<span class="dashicons dashicons-admin-generic"></span>
			<?php _e( 'DDF Custom JavaScript', 'scjs' ); ?>
		</h2>
		<div class="tips">
			<?php do_action( 'scjs-sidebar-top' ); ?>
			<p style="margin-top: 0"><?php _e( 'DDF Custom JavaScript allows you to add your own JavaScript or override the default JavaScript of a plugin or theme.', 'scjs' ) ?></p>
			<p><?php _e( 'To use, enter your custom JavaScript, then click "Update Custom JavaScript".  It\'s that simple!', 'scjs' ) ?></p>
			<?php submit_button( __( 'Update Custom JavaScript', 'scjs' ), 'primary', 'submit', true ); ?>
			<?php do_action( 'scjs-sidebar-bottom' ); ?>
		</div>

		<form name="scjs-form" action="options.php" method="post" enctype="multipart/form-data">

			<?php settings_fields( 'scjs_settings_group' ); ?>
				<?php do_action( 'scjs-form-top' ); ?>
				<div>
					<textarea name="scjs_settings[scjs-content]" id="scjs_settings[scjs-content]" ><?php echo esc_html( $content ); ?></textarea>
				</div>
				<?php do_action( 'scjs-textarea-bottom' ); ?>
				<div>
					<?php submit_button( __( 'Update Custom JavaScript', 'scjs' ), 'primary', 'submit', true ); ?>
				</div>
				<?php do_action( 'scjs-form-bottom' ); ?>

		</form>
		<script language="javascript">
			jQuery( document ).ready( function() {
				var editor = CodeMirror.fromTextArea( document.getElementById( "scjs_settings[scjs-content]" ), {lineNumbers: true, lineWrapping: true, matchBrackets: true} );
			});
		</script>
	</div>
<script src="<?=dirname(dirname(plugin_dir_url( __FILE__ ))) . '/vendor/codemirror/javascript.js'?>"></script>