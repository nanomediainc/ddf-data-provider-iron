<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   Plugin_Name
 * @author    Your Name <email@example.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Your Name or Company Name
 */
$options_display = get_option( 'ddf_display_options' );
?>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<div class="wrap">

        <h2>
            <span class="dashicons dashicons-admin-generic"></span>
            <?php echo esc_html( get_admin_page_title() ); ?>
        </h2>
    
        <form action="options.php" method="post">
            <div class="hidden">
                <input type="radio" class="hidden" value="*" name="minutes">
            </div>
            <?php settings_fields('ddf_display_options'); ?>
            <table class="wp-list-table display-setting">
                <thead>
                    <th class="header">Options</th>
                    <th class="header">Settings</th>
                </thead>
                <tbody>
                <tr>
                    <td class="option-title">Listings Per Page:</td>
                    <td class="option-content">

                            <label for="limit">listings:</label>
                            <input type="text" id="limit" style="border:0; color:#333333; width: 50px; font-weight:bold;">
                            <label for="limit">items per page</label>
                            <input type="hidden"
                                   id="listing_limit"
                                   name="ddf_display_options[listing_limit]"
                                   value="<?=idx($options_display,"listing_limit", 10 );?>">
                        
                        <div id="limiter"></div>  
                        
                    </td>
                </tr>                    
                <tr>
                    <td class="option-title">Display Agent:</td>
                    <td class="option-content">
                        <div class="switch-wrapper">
                        <input id="agent-option" 
                               type="checkbox" 
                               <?php
                                if( isset($options_display["show_agent"] ) )
                                {
                                    if( $options_display["show_agent"] == 1)
                                        echo 'value="1" checked';
                                    else
                                        echo 'value="0"';
                                }?> >
                        </div>
                       
                        <input id="show_agent" 
                               name="ddf_display_options[show_agent]"  
                               type="hidden" 
                               value="<?php echo isset( $options_display["show_agent"] ) 
                                                    ? $options_display["show_agent"] 
                                                    : ''; ?>">                        
                    </td>
                </tr>								
                <tr>
                    <td class="option-title">Image Size:</td>
                    <td class="option-content">
						 Width: 						
						<input id="image_size_width" 
                               name="ddf_display_options[image_size_width]"  
                               type="number"
							   size="10"
							   maxlength="4"
                               value="<?php echo (isset( $options_display["image_size_width"] ) 
															and !empty($options_display["image_size_width"]) )
                                                    ? $options_display["image_size_width"] 
                                                    : '480'; ?>">     
						<br/>						 
						Height:						 
                        <input id="image_size_height" 
                               name="ddf_display_options[image_size_height]"  
                               type="number"
							   size="10"
							   maxlength="4"
								 value="<?php echo ( isset( $options_display["image_size_height"] ) 
												 and !empty($options_display["image_size_height"] ) ) 
												  ? $options_display["image_size_height"]  
			 									  : '320'; ?>">										
                    </td> 
                </tr>
<tr>
                    <td class="option-title">Map Size:</td>
                    <td class="option-content">
						 Width: 						
						<input id="map_size_width" 
                               name="ddf_display_options[map_size_width]"  
                               type="number"
							   size="10"
							   maxlength="4"
                               value="<?php echo (isset( $options_display["map_size_width"] ) 
															and !empty($options_display["map_size_width"]) )
                                                    ? $options_display["map_size_width"] 
                                                    : '480'; ?>">     
						<br/>						 
						Height:						 
                        <input id="map_size_height" 
                               name="ddf_display_options[map_size_height]"  
                               type="number"
							   size="10"
							   maxlength="4"
								 value="<?php echo ( isset( $options_display["map_size_height"] ) 
												 and !empty($options_display["map_size_height"] ) ) 
												  ? $options_display["map_size_height"]  
												  : '320'; ?>">										
                    </td>
                </tr>
                <tr>
                    <td class="option-title">
								New Listing Range:<br><br><small>A listing will be considered "New Listing" within this chosen day range. </small>
					</td>
                    <td class="option-content">

                            <label for="amount">Choose new listing range:</label>
                            <input type="text" id="amount" style="border:0; color:#333333; width: 50px; font-weight:bold;">
                            <label for="amount">days</label>
                            <input type="hidden"
                                   id="new_listing_days"
                                   name="ddf_display_options[new_listing_days]"
                                   value="<?php echo isset( $options_display["new_listing_days"] ) ? $options_display["new_listing_days"] : '3'; ?>">

                        <div id="slider"></div>
                    </td>
                </tr>
                <tr>
                    <td class="option-title">New Listing Label:<br><br><small>The label image will display if the current listing is New Listing. </small></td>
                    <td class="option-content">
                        <p>
                            <label for="amount">Choose a label:</label>
                        </p>                        
                        <table class="widefat badge-wrapper">
                            
                            <tr>
                                <td>
                                    <ul class="badge">
                            <?php for($i=1; $i<=9; $i++): ?>        
                                    <li>
                                         <input type="radio" 
                                                value="<?php echo $i;?>" 
                                                class="new-label" 
                                                id="label_style"
                                                name="ddf_display_options[label_style]"
                                                <?php
                                                    if( isset( $options_display["label_style"] ) and $options_display["label_style"] == $i ) 
                                                            echo 'checked="checked"'; 
                                                ?>
                                                > 
                                         <img role="new-label-<?php echo $i; ?>" src="<?php echo  plugin_dir_url(dirname(dirname(__FILE__))).'upload/label/'.$i.'.png'; ?>">
                                         
                                     </li>                                
                             <?php endfor; ?>
                                    <ul>
                                <td>                                     
                             </tr>
                        </table>                           
                    </td>
                </tr>     
                <tr>
                    <td class="option-title">Preview</td>
                    <td class="option-content preview-thumbnail" align="center">
                        <div class="demo-img">
                            <div class="certs"></div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td class="option-title">Terms of Use</td>
                    <td class="option-content preview-thumbnail" align="center">
                        <textarea class="aggrement" name="ddf_display_options[aggrement]" rows="10"><?php
                            echo $options_display['aggrement'];
                            ?></textarea>
                    </td>
                </tr>
                </tbody>
            </table>
            <?php submit_button('Save Display Option','primary account-display-btn'); ?>
        </form>        
</div>
