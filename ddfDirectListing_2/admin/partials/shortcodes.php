<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-08-23
 * Time: 10:12 PM
 */
$agents = post_data(array('action'=>'paid_agent_details'));
$agents = json_decode($agents);

$offices = post_data(array('action'=>'sc_by_office'));
$offices = json_decode($offices);

$cities = post_data(array('action'=>'sc_by_city'));
$cities = json_decode($cities);

$ptypes = post_data(array('action'=>'sc_by_ptype'));
$ptypes = json_decode($ptypes);

$btypes = post_data(array('action'=>'sc_by_btype'));
$btypes = json_decode($btypes);

?>
<div class="wrap">
	<h2 style="margin-bottom: 1em;">
		<span class="dashicons dashicons-admin-generic"></span>
		<?php echo esc_html( get_admin_page_title() ); ?>
	</h2>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Universal Meta</h3>
		</div>
		<div class="panel-body">
			<pre>limit="2" // with any number</pre>
			<pre>show_all="true" // show all results</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">All Listings</h3>
		</div>
		<div class="panel-body">
			Always with View More button.
			<pre>[ddf2 type="all"]</pre>
			Extra key:
			<select class="city select2">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="city_meta_result">city="Toronto" //City meta</pre>
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Search Bar</h3>
		</div>
		<div class="panel-body">
			You can join this shortcode with other ones on the same page.
			<pre>[ddf2 type="search-bar"]</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
	<hr>
	<h3>DDF Agent Listings</h3>
	<hr>
	<div class="clear clearfix"></div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Agent Key</h3>
		</div>
		<div class="panel-body">
			<select class="agent_ids select2" multiple="multiple" style="width: 75%">
				<?php
					foreach($agents as $k=>$v){
						echo '<option value="'.$v->meta_value.'">'.$v->post_title.'</option>';
					}
				?>
			</select>
			<br>
			Show all listings (both):
			<pre class="agent_ids_result">[ddf2 type="selected_agent_listings" meta=""]</pre>

			Show active listings:
			<pre class="agent_ids_result_active">[ddf2 type="selected_agent_listings" meta="" listing_type="active"]</pre>

			Show past listings:
			<pre class="agent_ids_result_past">[ddf2 type="selected_agent_listings" meta="" listing_type="past"]</pre>
			Extra key:
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">All DDF Agents Active Listings</h3>
		</div>
		<div class="panel-body">
			<input type="checkbox"
			       class="check-box"
			       data-target="agent_listings_result"
			       data-on='[ddf2 type="agent_ddf_listings" show_all="true"]'
			       data-off='[ddf2 type="agent_ddf_listings"]'>Displays all.
			<pre class="agent_listings_result">[ddf2 type="agent_ddf_listings"]</pre>
			Extra key:
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">All DDF Agents Past Listings</h3>
		</div>
		<div class="panel-body">
			<input type="checkbox"
			       class="check-box"
			       data-target="agent_past_listings_result"
			       data-on='[ddf2 type="agent_past_listings" show_all="true"]'
			       data-off='[ddf2 type="agent_past_listings"]'>Displays all.

			<pre class="agent_past_listings_result">[ddf2 type="agent_past_listings"]</pre>
			Extra key:
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>

	<div class="clear clearfix"></div>
	<hr>
	<h3>Brokerage Listings</h3>
	<hr>
	<div class="clear clearfix"></div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Office ID</h3>
		</div>
		<div class="panel-body">
			You are able to select more than one option.
			<select class="office_ids select2" multiple="multiple" style="width: 75%">
				<?php
				foreach($offices as $k=>$v){
					echo '<option value="'.$v->OfficeID.'">'.ucwords($v->OfficeNAME).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="office_ids_result">[ddf2 type="office_listings" meta=""] // Show all office listing</pre>
			Extra key:
			<select class="city select2">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="city_meta_result">city="Toronto" //City meta</pre>
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>

	<div class="clear clearfix"></div>
	<hr>
	<h3>Geo Listings</h3>
	<hr>
	<div class="clear clearfix"></div>


	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">City</h3>
		</div>
		<div class="panel-body">
			<select class="cities select2" style="width: 75%">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="cities_result">[ddf2 type="city_listings" meta=""]</pre>
			Extra key:
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>


	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Postal Code</h3>
		</div>
		<div class="panel-body">
			<input class="form-control postalcode" placeholder="Postal Code: M9A,M5A,L7N" />
			<pre class="postal_result">[ddf2 type="selected_postalcode_listings" meta=""]</pre>
			Extra key:
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>

	<div class="clear clearfix"></div>
	<hr>
	<h3>Property Listings</h3>
	<hr>
	<div class="clear clearfix"></div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Property ID</h3>
		</div>
		<div class="panel-body">
			<input class="form-control property_id" placeholder="Property(s): 157354,1567481,168546" />
			<br>
			<pre class="property_id_result">[ddf2 type="selected_property_id" meta=""]</pre>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Property Status</h3>
		</div>
		<div class="panel-body">
			<select class="property_status select2" style="width: 75%">
				<option value="For sale">For Sale</option>
				<option value="For lease">For Lease</option>
			</select>
			<pre class="property_status_result">[ddf2 type="listing_type" meta=""]</pre>
			Extra key:
			<select class="city select2">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="city_meta_result">city="Toronto" //City meta</pre>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Property Type</h3>
		</div>
		<div class="panel-body">
			<select class="property_type select2" multiple="multiple" style="width: 75%">
				<?php
				foreach($ptypes as $k=>$v){
					echo '<option value="'.$v->PropertyType.'">'.ucwords($v->PropertyType).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="property_type_result">[ddf2 type="selected_property_type" meta=""]</pre>
			Extra key:
			<select class="city select2">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="city_meta_result">city="Toronto" //City meta</pre>
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Building Type</h3>
		</div>
		<div class="panel-body">
			<select class="building_type select2" multiple="multiple" style="width: 75%">
				<?php
				foreach($btypes as $k=>$v){
					echo '<option value="'.$v->BuildingType.'">'.ucwords($v->BuildingType).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="building_type_result">[ddf2 type="selected_building_type" meta=""]</pre>
			Extra key:
			<select class="city select2">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="city_meta_result">city="Toronto" //City meta</pre>
			<pre class="list_type_meta_result">list_type="Sale" //Rent or Sale</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
	<hr>
	<h3>Price Range Listings</h3>
	<hr>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Price Range</h3>
		</div>
		<div class="panel-body">
			Min
			<select class="min_price price_range select2">
				<option value="" selected>Min Price Limit</option>
				<option value="25000" role="min">$25,000</option>
				<option value="50000">$50,000</option>
				<option value="75000">$75,000</option>

				<option value="100000">$100,000</option>
				<option value="125000">$125,000</option>
				<option value="150000">$150,000</option>
				<option value="175000">$175,000</option>

				<option value="200000">$200,000</option>
				<option value="225000">$225,000</option>
				<option value="250000">$250,000</option>
				<option value="275000">$275,000</option>

				<option value="300000">$300,000</option>
				<option value="325000">$325,000</option>
				<option value="350000">$350,000</option>
				<option value="375000">$375,000</option>

				<option value="400000">$400,000</option>
				<option value="450000">$450,000</option>
				<option value="475000">$475,000</option>

				<option value="500000">$500,000</option>
				<option value="550000">$550,000</option>
				<option value="575000">$575,000</option>

				<option value="600000">$600,000</option>
				<option value="650000">$650,000</option>
				<option value="675000">$675,000</option>

				<option value="700000">$700,000</option>
				<option value="750000">$750,000</option>
				<option value="775000">$775,000</option>

				<option value="800000">$800,000</option>
				<option value="850000">$850,000</option>

				<option value="900000">$900,000</option>
				<option value="950000">$950,000</option>

				<option value="1000000">$1,000,000</option>
				<option value="1100000">$1,100,000</option>
				<option value="1200000">$1,200,000</option>
				<option value="1300000">$1,300,000</option>
				<option value="1400000">$1,400,000</option>
				<option value="1500000">$1,500,000</option>
				<option value="1600000">$1,600,000</option>
				<option value="1700000">$1,700,000</option>
				<option value="1800000">$1,800,000</option>
				<option value="1900000">$1,900,000</option>

				<option value="2000000">$2,000,000</option>
				<option value="2500000">$2,500,000</option>
				<option value="3000000">$3,000,000</option>
				<option value="3000000">$3,000,000</option>
				<option value="4000000">$4,000,000</option>
				<option value="5000000">$5,000,000</option>
				<option value="7500000">$7,500,000</option>
				<option value="10000000">$10,000,000</option>
			</select>
			Max

			<select class="max_price price_range select2">
				<option value="" selected>Max Price Limit</option>

				<option value="25000">$25,000</option>
				<option value="50000">$50,000</option>
				<option value="75000">$75,000</option>

				<option value="100000">$100,000</option>
				<option value="125000">$125,000</option>
				<option value="150000">$150,000</option>
				<option value="175000">$175,000</option>

				<option value="200000" role="max">$200,000</option>
				<option value="225000">$225,000</option>
				<option value="250000">$250,000</option>
				<option value="275000">$275,000</option>

				<option value="300000">$300,000</option>
				<option value="325000">$325,000</option>
				<option value="350000">$350,000</option>
				<option value="375000">$375,000</option>

				<option value="400000">$400,000</option>
				<option value="450000">$450,000</option>
				<option value="475000">$475,000</option>

				<option value="500000">$500,000</option>
				<option value="550000">$550,000</option>
				<option value="575000">$575,000</option>

				<option value="600000">$600,000</option>
				<option value="650000">$650,000</option>
				<option value="675000">$675,000</option>

				<option value="700000">$700,000</option>
				<option value="750000">$750,000</option>
				<option value="775000">$775,000</option>

				<option value="800000">$800,000</option>
				<option value="850000">$850,000</option>

				<option value="900000">$900,000</option>
				<option value="950000">$950,000</option>

				<option value="1000000">$1,000,000</option>
				<option value="1100000">$1,100,000</option>
				<option value="1200000">$1,200,000</option>
				<option value="1300000">$1,300,000</option>
				<option value="1400000">$1,400,000</option>
				<option value="1500000">$1,500,000</option>
				<option value="1600000">$1,600,000</option>
				<option value="1700000">$1,700,000</option>
				<option value="1800000">$1,800,000</option>
				<option value="1900000">$1,900,000</option>

				<option value="2000000">$2,000,000</option>
				<option value="2500000">$2,500,000</option>
				<option value="3000000">$3,000,000</option>
				<option value="3000000">$3,000,000</option>
				<option value="4000000">$4,000,000</option>
				<option value="5000000">$5,000,000</option>
				<option value="7500000">$7,500,000</option>
				<option value="10000000">$10,000,000</option>
			</select>
			Property
			<select class="sort_price price_range select2">
				<option value="" selected>Status</option>
				<option value="sale" >For Sale</option>
				<option value="other" >For Rent/Lease</option>
				<option value="all" >Both</option>
			</select>
			<pre class="price_range_result">[ddf2 type="selected_price"]</pre>
			Extra key:
			<select class="city select2">
				<?php
				foreach($cities as $k=>$v){
					echo '<option value="'.$v->City.'">'.ucwords($v->City).' ('.$v->total.')</option>';
				}
				?>
			</select>
			<pre class="city_meta_result">city="Toronto" //City meta</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
	<hr>
	<h3>Manual Listings</h3>
	<hr>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Exclusive Listings</h3>
		</div>
		<div class="panel-body">
			<pre class="office_ids_result">[ddf2 type="manual_listing" category="exclusive"]</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Past Listings</h3>
		</div>
		<div class="panel-body">
			<pre class="office_ids_result">[ddf2 type="manual_listing" category="past"]</pre>
		</div>
	</div>
	<div class="clear clearfix"></div>
</div>