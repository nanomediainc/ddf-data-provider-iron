<?php
/**
 * Plugin Name:       DDF Direct Listing 2
 * Plugin URI:        http://www.realestateapps.ca
 * Description:       DDF Direct Listing is a WordPress plug-in to enable CREA’s members to directly disseminate MLS® listing content on WordPress Sites.
 * Version:           2.0.0
 * Author:            Tony Lee
 * Author URI:        http://nanomedia.ca/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ddfTech
 * Domain Path:       /languages
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}
setlocale(LC_MONETARY, 'en_CA.UTF-8');
date_default_timezone_set('Canada/Central');
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ddf-direct-listing-activator.php
 */
register_activation_hook( __FILE__, function () {

	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ddf-direct-listing-activator.php';

	DDF_Direct_Listing_Activator::activate();

});

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ddf-direct-listing-deactivator.php
 */
register_deactivation_hook( __FILE__, function() {

	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ddf-direct-listing-deactivator.php';

	DDF_Direct_Listing_Deactivator::deactivate();

} );


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ddf-direct-listing.php';
require plugin_dir_path( __FILE__ ) . 'includes/helper.php';

run_plugin_name();