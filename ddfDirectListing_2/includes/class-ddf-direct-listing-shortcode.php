<?php
/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    DDF_Direct_Listing
 * @subpackage Plugin_Name/includes
 * @author     Tony Lee <var.tonylee@gmail.com>
 */
class DDF_Direct_Listing_Shortcode {


	public function __construct() {

	}

	/**
	 * Handel DDF shortcode
	 *
	 * @since    1.0.0
	 */
	public function ddf_shortcode_handler($atts, $content){

		$atts= shortcode_atts(array(
			'feeds'     => !empty($atts) ? $atts : 'all',
			'content'   => !empty($content) ? $content : '1'
		), $atts);

		if( is_string($atts['feeds']) ){
			$atts['feeds'] = array('type' => $atts['feeds']);
		}

		list($this->feed_name, $this->feed_content) = array_values($atts);

		if(isset($_GET['pid'])) {
			$data = $this->post_data;
			$page = 'details.php';
			$type = 'single';
		}
		else{
			$page = 'listing.php';
			$type = 'all';
		}

		if($atts['feeds']['type'] == 'agent_ddf_listings'){
			$post_type = 'agent_ddf_listings';
			#$this->post_data = $this->getDisplayingResult( array( 'type' => $type ), $post_type, 0, 20 );

			#}elseif($atts['feeds']['type'] == 'agent_ddf_listings_sold'){
			#$post_type = 'agent_ddf_listings';
			$this->feed_name['type'] = $post_type;
			$this->post_data = $this->getDisplayingResult( array( 'type' => $type ), $post_type, 0, 20, 'both');

		}else{
			$this->post_data = $this->getDisplayingResult( array( 'type' => $type ) );

		}



		$class_name = 'list';
		if( isset($this->options_display) and is_array($this->options_display) ) {
			extract( $this->options_display );
			if($layout_option == 1){

				$class_name = 'box';

			}
		}

		include_once( 'views/'.$page );
		// Add Custom CSS
		custom_css_ob();

		// Add Custom JS
		if ( isset($this->options_display['custom_js']) and !empty( $this->options_display['custom_js']) ) {
			custom_js_ob( $this->options_display['custom_js'] );
		}

	}
}