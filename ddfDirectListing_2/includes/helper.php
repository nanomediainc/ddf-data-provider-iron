<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-08-28
 * Time: 12:47 AM
 */

function post_data( $data, $uri='' ) {

	switch($uri){

		case 'listing';
			$uri = 'http://realtechpro.io/_ddf_handler/fetch_listing.php?v='.time();
			$data["token"] = "Vp9+8%3+^=9||=2kJ+T4179^818:C3";
			break;

		case 'single';
			$uri = 'http://realtechpro.io/_ddf_handler/single.php';
			$data["token"] = "Vp9+=9||=Vp9+8%3+^=9||18:C38%3+^";
			break;

		case 'checker';
			$uri = 'http://realtechpro.io/_ddf_handler/chk.php';
			$data["token"] = "8%3+^=9||18:C38+8%3+^=979^818:C3V";
			break;

		default:
			$uri = 'http://realtechpro.io/_ddf_handler/fetch_all.php';
			$data["token"] = "+T4179^818:C3Vp9+8%3+^=9||=2kJ";
	}

	try {
		$curl = curl_init( $uri );
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30 );

		$response = curl_exec( $curl );

		curl_close( $curl );

	}catch (Exception $e){
		echo $e->getMessage();
	}
	return $response;
}

function idx($array, $key, $default = null) {

	if( is_object($array)){
		$array = (array)$array;
	}

	if( !isset($array) or (is_array($array) === false) or !isset($array[$key]) ){
		return $default;
	}

	return $array[$key];
}

function std_to_array( $std ){
	return json_decode( json_encode($std), true );
}
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */

function run_plugin_name() {

	$plugin = new DDF_Direct_Listing();
	$plugin->run();
}


function trace() {
	$trace  = debug_backtrace();
	$caller = $trace[1];

	error_log( "Called by {$caller['function']}" );
	if ( isset( $caller['class'] ) ) {
		error_log( " in {$caller['class']}" );
	}
	error_log( '==========' );
}
if( !function_exists('print_array')) {
	function print_array( $array, $exit = false ) {

		echo '<pre>';
		print_r( $array );
		echo '</pre>';

		if ( $exit == true ) {
			exit();
		}

	}
}
function getHowLongAgo($date)
{
	$datetime1 = new DateTime( date('Y-m-d') );
	$datetime2 = new DateTime( date('Y-m-d', strtotime($date)) );
	$interval = $datetime1->diff($datetime2);


	if( 0 < $interval->invert )
		return $interval->days;
	else
		return 0;
}
function splitAtUpperCase($s) {
	return preg_split('/(?=[A-Z])/', $s, -1, PREG_SPLIT_NO_EMPTY);
}