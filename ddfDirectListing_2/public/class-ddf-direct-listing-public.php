<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    DDF_Direct_Listing
 * @subpackage Plugin_Name/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    DDF_Direct_Listing
 * @subpackage Plugin_Name/public
 * @author     Tony Lee <var.tonylee@gmail.com>
 */
class DDF_Direct_Listing_Public {

	/**
	 *  The public variable of this plugin
	 */

	public $options_display;
	public $remove_uri;
	public $plugin_uri;
	public $view_type;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$opt = '';

		$opt = get_option( 'ddf_display_options' );

		$this->options_display = $opt;
		$this->remove_uri =  'http://realtechpro.io/_ddf_handler';
		$this->plugin_uri =  plugin_dir_url( dirname(  __FILE__ )  );
		$this->last_id    =  0;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ddf-direct-listing-public.css', array(), $this->version, 'all' );
		wp_enqueue_style(
			$this->plugin_name.'-datepicker',
			dirname(plugin_dir_url( __FILE__ )) . '/vendor/datepicker/bootstrap-datepicker3.standalone.min.css',
			array(),
			$this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script(
			$this->plugin_name,
			plugin_dir_url( __FILE__ ) . 'js/ddf-direct-listing-public.js',
			array( 'jquery', 'jquery-ui-core', 'jquery-ui-slider' ),
			$this->version, false
		);

		wp_enqueue_script(
			$this->plugin_name.'-jcarousel',
			dirname(plugin_dir_url( __FILE__ )) . '/vendor/jquery/jquery.jcarousel.min.js',
			array( 'jquery' ),
			$this->version,
			false
		);

		wp_enqueue_script(
			$this->plugin_name.'-datepicker',
			dirname(plugin_dir_url( __FILE__ )) . '/vendor/datepicker/bootstrap-datepicker.min.js',
			array( 'jquery' ),
			$this->version,
			false
		);
	}

	/**
	 * Handel DDF shortcode
	 *
	 * @since    1.0.0
	 */
	public function ddf_shortcode_handler($atts, $content){

		/*
		 * Return:
		 *  $short_code  -- Array ( [type] => post [meta] => 123 )
		 *  $short_content    -- content of the shortcode
		 *
		 * */
		extract($atts= shortcode_atts(array(
			'short_code'     => !empty($atts) ? $atts : false,
			'shortcontent'   => !empty($content) ? $content : false
		), $atts));

		if( !isset($short_code['type'])){
			return;
		}

		if($short_code['type'] == 'search-bar'){
			# Display Search bar
			$data = true;
			$page = 'search.php';

		}elseif(isset( $_POST['search']) ){
			# Add search data to build the query
			$data = $this->form_post( $short_code );
			$short_code['type'] = 'all';
			$page = 'listing.php';

		}else {
			# For other shortcodes
			list($data, $page, $pass_query) = $this->shortcode_action( $short_code );
		}

		# Out put the results
		if($data) {

			ob_start();
			include( 'partials/' . $page );
			$html = ob_get_clean();

			# Add Custom CSS and JavaScript
			$css = $this->custom_css_ob();
			$js  = $this->custom_js_ob();

			return  $html . $css . $js;

		}

	}

	public function shortcode_action( $short_code ){

		$pass_query = $data = '';

		$limit = idx($short_code, 'show_all', idx($short_code, 'limit', $this->options_display['listing_limit']));
		$city  = idx($short_code, 'city', '');

		$list_type  = idx($short_code, 'list_type', '');

		if( !empty($list_type) ){
			$param['list_type'] = $list_type;
			$pass_query = array( 'list_type' => $list_type );
		}

		if($limit == "true"){
			$limit = 18446744073709551610;
		}

		switch ( $short_code['type'] ) {

			case 'all':
			case 'agent_ddf_listings':
			case 'agent_past_listings':

				$param['action'] = 'fn_' . trim( $short_code['type'] );
				$param['limit']  = $limit;

				if ( $short_code['type'] == 'all' and !empty( $city ) ) {
						$param['city'] = $city;
				}

				$data = post_data( $param, 'listing' );
				$page = 'listing.php';

				break;

			case 'single';

				global $wp_query;
				$page_id = $wp_query->query_vars['page'];

				if ( $page_id ) {
					if ( substr( $page_id, 0, 6 ) != '990990' ) {
						$data = post_data( array(
							'action'    => 'fn_single',
							'ListingID' => $wp_query->query_vars['page']
						), 'listing' );
						$page = 'details.php';
					} else {
						$data = get_meta_box_content( substr( $page_id, 6 ), 'ddf_manual_listing', '', false );
						$page = 'custom-details.php';
					}
				}
				break;
			case 'selected_agent_listings':


				$param['action']   = 'fn_' . trim( $short_code['type'] );
				$param['AgentKey'] = $short_code['meta'];
				$param['limit']    = $limit;

				$res   = idx( $short_code, 'listing_type', '' );
				if ( ! empty( $res ) ) {
					$param['type'] = $short_code['listing_type'];
				}

				$data = post_data( $param, 'listing' );
				$page = 'listing.php';
				break;

			case 'office_listings':

				$param['action']    = 'fn_' . trim( $short_code['type'] );
				$param['office_id'] = $short_code['meta'];
				$param['limit']     = $limit;
				$pass_query['meta'] = $short_code['meta'];

				if ( ! empty( $city ) ) {
					$param['city'] = $city;
					$pass_query['city'] = $city;
				}

				$data       = post_data( $param, 'listing' );

				$page = 'listing.php';

				break;

			case 'city_listings':

				$param['action'] = 'fn_' . trim( $short_code['type'] );
				$param['city']   = $short_code['meta'];
				$param['limit']  = $limit;

				$data               = post_data( $param, 'listing' );
				$pass_query['meta'] = $short_code['meta'];

				$page = 'listing.php';
				break;

			case 'selected_postalcode_listings':

				$param['action']      = 'fn_' . trim( $short_code['type'] );
				$param['postal_code'] = $short_code['meta'];
				$param['limit']       = $limit;
				$data                 = post_data( $param, 'listing' );
				$pass_query['meta']   = $short_code['meta'];

				$page = 'listing.php';
				break;

			case 'selected_property_id':

				$param['action']      = 'fn_' . trim( $short_code['type'] );
				$param['property_id'] = $short_code['meta'];

				$data  = post_data( $param, 'listing' );
				$page  = 'listing.php';
				break;

			case 'selected_property_type':

				$param['action']        = 'fn_' . trim( $short_code['type'] );
				$param['property_type'] = $short_code['meta'];
				$param['limit']         = $limit;
				$pass_query['meta'] = $short_code['meta'];

				if ( ! empty( $city ) ) {
					$param['city']      = $city;
					$pass_query['city'] = $short_code['city'];
				}

				$data = post_data( $param, 'listing' );

				$page = 'listing.php';
				break;

			case 'listing_type':

				$param['action']       = 'fn_' . trim( $short_code['type'] );
				$param['listing_type'] = $short_code['meta'];
				$param['limit']        = $limit;

				$pass_query['meta'] = $short_code['meta'];
				if ( ! empty( $city ) ) {
					$param['city']      = $city;
					$pass_query['city'] = $short_code['city'];
				}
				$data = post_data( $param, 'listing' );
				$page = 'listing.php';
				break;

			case 'selected_building_type';


				$param['action']        = 'fn_' . trim( $short_code['type'] );
				$param['building_type'] = $short_code['meta'];
				$param['limit']         = $limit;
				$pass_query['meta']     = $short_code['meta'];

				if ( ! empty( $city ) ) {
					$param['city']      = $city;
					$pass_query['city'] = $short_code['city'];
				}
				$data = post_data( $param, 'listing' );
				$page = 'listing.php';
				break;

			case 'selected_price':

				$param['action'] = 'fn_' . trim( $short_code['type'] );
				$param['limit']  = $limit;

				$max    = idx( $short_code, 'max', '' );
				$min    = idx( $short_code, 'min', '' );
				$status = idx( $short_code, 'status', '' );

				if ( ! empty( $max ) ) {
					$param['max']      = $max;
					$pass_query['max'] = $max;
				}
				if ( ! empty( $min ) ) {
					$param['min']      = $min;
					$pass_query['min'] = $min;
				}
				if ( ! empty( $status ) ) {
					$param['status']      = $status;
					$pass_query['status'] = $status;
				}
				if ( ! empty( $city ) ) {
					$param['city']      = $city;
					$pass_query['city'] = $city;
				}

				$data = post_data( $param, 'listing' );

				$page = 'listing.php';

				break;

			case 'manual_listing':

				$param['post_type']        = 'ddf_manual_listing';
				$param['post_status']      = 'publish';
				$param['suppress_filters'] = false;
				$param['orderby']          = 'post_date';
				$param['order']            = 'DESC';

				$data = get_posts( $param );

				$page = 'custom_listing.php';

				break;
		}

		return array($data, $page, $pass_query);

	}
	public function form_post( $short_code ){

		$limit = idx($short_code, 'show_all', idx($short_code, 'limit', $this->options_display['listing_limit']));

		if($limit == "true"){
			$limit = 18446744073709551610;
		}

		$param = array(
			'action' => 'fn_all',
			'limit'  => $limit
		);

		$param['data'] = urlencode( json_encode( $_POST ) );
		$data = post_data( $param,	'listing' );
		return $data;
	}

	public function custom_css_ob() {

		$options     = get_option( 'sccss_settings' );
		$raw_content = isset( $options['sccss-content'] ) ? $options['sccss-content'] : '';
		$content     = wp_kses( $raw_content, array( '\'', '\"' ) );
		$content     = str_replace( '&gt;', '>', $content );
		return '<style>' . $content .'</style>';
	}

	public function custom_js_ob() {

		$options     = get_option( 'scjs_settings' );
		$raw_content = isset( $options['scjs-content'] ) ? $options['scjs-content'] : '';
		$content     = wp_kses( $raw_content, array( '\'', '\"' ) );
		$content     = str_replace( '&gt;', '>', $content );

		return '<script>' . $content .'</script>';
	}

	public function build_single_page(){

		if(!get_page_by_title('Property')){

			$_p = array();
			$_p['post_title']   = 'Property';
			$_p['post_content'] = '[ddf2 type="single"]';
			$_p['post_status']  = 'publish';
			$_p['post_type']    = 'page';
			$_p['comment_status']   = 'closed';
			$_p['ping_status']      = 'closed';
			$_p['post_category']    = array(1);

			$blog_id = get_current_blog_id();
			if ( is_multisite() ) {
				switch_to_blog( $blog_id );
				$the_page_id = wp_insert_post( $_p );
				restore_current_blog();
			}else{
				$the_page_id = wp_insert_post( $_p );
			}
		}
	}
	public function add_admin_url() {
		echo '<script>var ajaxurl = ajaxurl || "' . admin_url( 'admin-ajax.php' ) . '";</script>';
	}

	public function infinit_content(){

		$data = $_POST['data'];

		extract($this->options_display);

		$param = array(  'action'=> 'fn_' . $data['type'] ,
		                 'limit' => $this->options_display['listing_limit'],
		                 'offset'=> $data['offset']
		);
		# Add search data to build the query
		if( isset($data['data']) ){
			$param['data'] = $data['data'];
		}

		$offset = $this->options_display['listing_limit'] + $data['offset'];
		#error_log( print_r($param,1) );

		$data = post_data( $param,'listing' );

		if($data) {
			$listing = std_to_array( json_decode( $data ) );
			ob_start();
			require_once( 'partials/listing-template.php' );
			$html = ob_get_clean();
		}else{
			$html= '';
		}

		echo json_encode( array( 'html' => $html, 'offset' => $offset  ) );

		die();
	}
}
