(function ( $ ) {
	$(function () {

		// Load more button
		var update_btn = function(offset){
			$( ".fa-spinner").css('display', 'none');
			$('.more-btn').attr('data-offset',offset);
		};

		$(document).on('click','.more-btn',  function(){

			$( ".fa-spinner").css('display', 'inline-block');
			var param =  {
				'offset': $(this).attr('data-offset'),
				'type'	: $(this).attr('data-type')
			};
			if( $(this).attr('data-query') ){
				param.data = $(this).attr('data-query');
			}
			$.post(ajaxurl,{
				'action' : 'get_posts',
				'data'   : param
			},function(response){

				var result = JSON && JSON.parse(response) || $.parseJSON(response);

				if(result.html == '')
					$( ".view-more" ).html( '<h3 class="no-result">No more data</h3>' );
				else{
					$( ".view-more" ).before( result.html );
					update_btn( result.offset );
				}
			});

		});

		$( '.term-action' ).on( 'click touchstart', function( event ) {

			if($(this).hasClass( "accept" )){
				document.cookie = $('.ddf-direct-listing').data('cookie') +'=ddf_direct_listing_cookie; expires=Thu, 18 Dec 2100 12:00:00 UTC; path=/';
				$(".terms.overlay-open").remove();

			}else{
				alert('You are not able to browse listing');
			}
		});

		function getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
			}
			return "";
		}

		if( getCookie($('.ddf-direct-listing').data('cookie')) ){

			$(".terms.overlay-open").remove();
		}

		$(".terms.overlay-open").each(function( index ) {
			console.log( index + ": " + $( this ).attr('id') );
			if(index > 0){
				$(this).children('.term-details').remove();
			}
		});

		$('.input-mask').click(function(){
			$(this).hide();
			$('#keyword').show().focus().focusout(function(){
				if($(this).val() == ''){
					$('.input-mask').show();
					$(this).hide();
				}
			});
		});

		$('#StartDate').datepicker({
			format: 'yyyy-mm-dd'
		});

	});
}(jQuery));