<?php

do_action ( 'wp_enqueue_scripts' );

$meta = $data;

$options_display  = get_option('ddf_display_options');
$img_w = idx( $options_display, 'image_size_width', 480 ).'px';;
$img_h = idx( $options_display, 'image_size_height', 320 ).'px';;

$map_img_w = idx( $options_display, 'map_size_width', 480 ).'px';
$map_img_h = idx( $options_display, 'map_size_height', 300 ).'px';

if( wp_is_mobile() ){
    $map_img_w='100%';
}
if(current($meta['Property_Category']) == 'Past'){
    $sold = true;
}
$list = array();
$list['Category'] = isset($meta['Property_Category']) ? current($meta['Property_Category']) : '';
$list['Bedrooms'] = isset($meta['Bedrooms']) ? current($meta['Bedrooms']) : '0' ;
$list['Bathrooms']    = isset($meta['Bathrooms']) ? current($meta['Bathrooms']) : '0';
$list['Price']  = isset($meta['Property_Price']) ? current($meta['Property_Price']) : '';
$list['Desc']   = isset($meta['Property_Desc']) ? current($meta['Property_Desc']) : '';
$list['GEO']    = isset($meta['geo_address']) ? current($meta['geo_address']) : '';
$list['Gallery']  = isset($meta['property_gallery']) ? array_values(maybe_unserialize( current($meta['property_gallery']) )) : 0;

?>

<div class="ddf-direct-listing details">

    <?php if( wp_is_mobile() ): ?>
    <div class="jcarousel-wrapper" style="width:100%;height:320px;">
        <div class="jcarousel">
            <ul>
                <?php if( $list['Gallery'] == 0): ?>
                        <li>
                            <img src="<?=$this->plugin_uri . 'upload/house_holder.png'?>" style="height:300px;">
                        </li>
                <?php else: ?>
                    <?php  foreach( $list['Gallery'] as $img):   ?>
                        <li>
                            <img src="<?=$img?>" style="height:300px;">
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <a href="#" class="jcarousel-control-prev prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next next">&rsaquo;</a>
            <p class="jcarousel-pagination"></p>
        </div>
    </div>
    <?php else: ?>
    <div class="jcarousel-wrapper" style="width:<?=$img_w?>; height:<?=$map_img_h?>;">
        <div class="jcarousel">
            <ul>
                <?php if( $list['Gallery'] == 0): ?>
                    <li>
                        <img src="<?=$this->plugin_uri . 'upload/house_holder.png'?>" style="width:<?=$img_w?>; height:<?=$img_h?>;">
                    </li>
                <?php else: ?>
                    <?php  foreach( $list['Gallery'] as $img):   ?>
                        <li>
                            <img src="<?=$img?>" style="width:<?=$img_w?>; height:<?=$img_h?>;">
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <a href="#" class="jcarousel-control-prev prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next next">&rsaquo;</a>
            <p class="jcarousel-pagination"></p>
        </div>
    </div>
    <?php endif; ?>


    <div id="map-overlay" class="google-maps" style='width:<?=$map_img_w?>; height:<?=$map_img_h?>;'>
            <iframe id="map" class="scrolloff" frameborder="0"  scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?php echo urlencode($list['GEO']); ?>&output=embed&iwloc=near"></iframe>
    </div>
	<div class="house-details">
        <div class="table-row"> 
            <div class="table price-info">
                <h1 class="text-left title">
                    <?php
                      if( $sold ){
	                      echo 'Past Listing';
                      }else {
                          if( isset($list['Price']) and $list['Price'] > 0) {
                              echo '$'. str_replace('.00','',$list['Price']);
                          }
                          elseif( isset($data['Price']) ){
                              echo money_format( '%.0n', (double) $data['Price'] );
                          }
                      }
                      ?>            
                </h1>
                <span class="span_block address"><?=$list['GEO']?></span>
            </div>
        </div>
        <div class="table-row">
            <div class="table quick-view">
                <div class="d-row">
                    <span class="span_block">Bathrooms: </span>
                    <span><?=$list['Bathrooms']?></span>
                </div>
                <div class="d-row">
                    <span class="span_block">Bedrooms: </span>
                    <span><?=$list['Bedrooms']?></span>
                </div>
            </div>
        </div>
        <div class="table-row">
            <div class="table intro">
                <h4 class="text-left span_block">Property Description</h4>
                <p class="details"><?php 
                    $content = $list['Desc'];
                    $content = str_replace('**** EXTRAS ****', '</p>* EXTRAS *<p>', $content );
                    echo $content;
                ?></p>
            </div>					
        </div>

	<div class="d-row text-center">
		<img src="<?php echo  plugin_dir_url( dirname( dirname( __FILE__ ) ) ); ?>public/img/REALTOR-MLS-logos.jpg" width="150" height="auto"><br>
		<span>The trademarks MLS®, REALTOR®, and the associated logos are owned or controlled by The Canadian Real Estate Association.  Used under license.</span>
	</div>
</div>

</div>

<script>
    (function($) {
        $(function() {
            $('.jcarousel').jcarousel();

            $('.jcarousel-control-prev')
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .jcarouselControl({
                    target: '-=1'
                });

            $('.jcarousel-control-next')
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .jcarouselControl({
                    target: '+=1'
                });

            $('#map-overlay').on("mouseup",function(){
                $('#map').addClass('scrolloff');
            }).on("mousedown",function(){
                $('#map').removeClass('scrolloff');
            });

            $("#map").mouseleave(function () {
                $('#map').addClass('scrolloff');

            });

        });
    })(jQuery);

</script>
<?php wp_footer(); ?>

<?php
	$content = simplexml_load_string(
		'<JS>'.$meta['AnalyticsClick'].'</JS>'
	);
	echo (string) $content;

	$content = simplexml_load_string(
		'<JS>'.$meta['AnalyticsView'].'</JS>'
	);
	echo (string) $content;