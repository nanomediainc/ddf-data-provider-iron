<div class="ddf-direct-listing" data-cookie="<?php echo $cookie_name; ?>">
	<div class="listings">
		<div class="listing-wrapper">
			<?php

			$remove_uri =  $this->remove_uri;
			$plugin_uri =  $this->plugin_uri;
			$listing = std_to_array(  $data );

			foreach ( $listing as $key => $value):
				if($short_code['category'] == 'past'){
					$sold = true;
				}

				$meta = get_meta_box_content( $value['ID'], 'ddf_manual_listing', '', false);
				$list['Expired_Date']  = isset($meta['Expired_Date']) ? current($meta['Expired_Date']) : '0';

				if( $list['Expired_Date'] > 0 and $list['Expired_Date'] < time() ){
					continue;
				}
				$list['Category'] = isset($meta['Property_Category']) ? current($meta['Property_Category']) : '';
				if( $short_code['category'] !=  strtolower($list['Category']) ){
					continue;
				}
				$list['Bedrooms'] = isset($meta['Bedrooms']) ? current($meta['Bedrooms']) : '0' ;
				$list['Bathrooms']    = isset($meta['Bathrooms']) ? current($meta['Bathrooms']) : '0';
				$list['Price']  = isset($meta['Property_Price']) ? current($meta['Property_Price']) : '';
				$list['Desc']   = isset($meta['Property_Desc']) ? current($meta['Property_Desc']) : '';
				$list['GEO']    = isset($meta['geo_address']) ? current($meta['geo_address']) : '';
				$list['Gallery']  = isset($meta['property_gallery']) ? array_values(maybe_unserialize( current($meta['property_gallery']) )) : array();

				?>
				<div class="wrapper">
					<div class="list" id="<?=$value['ID']?>">
						<div class="image-wrapper img-responsive ">
							<a href="/property/990990<?php echo $value['ID']; ?>" class="img" style="background-image: url('<?php
							if( count($list['Gallery']) > 0 ){
								echo current($list['Gallery']);
							}else{
								echo $plugin_uri . 'upload/house_holder.png';
							}
							?>');">
							</a>

						</div>
						<div class="sum-info img-responsive">

							<h3 class="price"><?php
								if( $sold == true){
									echo '<span class="sold"><a href="/property/990990'.$value['ID'].'">Past Listing</a></span>';
								}else {

									echo '<a href="/property/990990'.$value['ID'].'">';
									if( $list['Price'] ) {
										echo '$'. str_replace('.00','',$list['Price']);
									}
									echo '</a>';
								}
								?></h3>
							<ul class="room-details">
								<li class="bedroom icon">
									<span class="badge"><?=$list['Bedrooms']?></span>
								</li>
								<li class="bathroom icon">
									<span class="badge"><?=$list['Bathrooms']?></span>
								</li>
							</ul>
							<h5 class="address"><?=$value['post_title'];?></h5>
							<?php if( !wp_is_mobile() ): ?>
								<span class="intro"><?php
									echo substr( $list['Desc'], 0, 400).'... <a href="/property/990990'.$value['ID'].'" class="read-more"> Read More </a>';
									?></span>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

			<div class="clear"></div>
		</div>
	</div>
</div>