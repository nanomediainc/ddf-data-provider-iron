<?php

do_action ( 'wp_enqueue_scripts' );
$listing = std_to_array( json_decode( $data ) );
$data = current( $listing['data'] );
$meta = current( $listing['meta'] );
$meta = std_to_array(json_decode($meta['meta_value']));
$img_0_path = '/photos/' . $data['PropertyID'] . '/' . $data['PropertyID'] . '_1.jpg';

$options_display  = get_option('ddf_display_options');

$img_w = idx( $options_display, 'image_size_width', 480 ).'px';;
$img_h = idx( $options_display, 'image_size_height', 320 ).'px';;

$map_img_w = idx( $options_display, 'map_size_width', 480 ).'px';
$map_img_h = idx( $options_display, 'map_size_height', 300 ).'px';

if( wp_is_mobile() ){
    $map_img_w='100%';
}

$openHouseDate = array();
$single = post_data( array('id'=>$data['PropertyID']) ,'single' );
if($single) {
    $single = std_to_array( json_decode( $single ) );
    if(isset($single['OpenHouse'])) {
        $openHouseDate = $single['OpenHouse']['Event'];
    }
}
if( isset($data['OpenHouse']) and $data['OpenHouse'] > 0 ){
    $openHouseDate = $single['OpenHouse']['Event'];
}

//----------------------------
//----- overlay --------------
//----------------------------
global $blog_id;

$listing = std_to_array( $data );
$cookie_name = 'ddf_direct_listing_cookie_'.$blog_id;
$my_option = '';
//----------------------------
?>

    <div class="ddf-direct-listing details" data-cookie="<?php echo $cookie_name; ?>">
        <?php if( wp_is_mobile() ): ?>
            <div class="jcarousel-wrapper" style="width:100%;height:320px;">
                <div class="jcarousel">
                    <ul>
                        <?php
                        $counter = 0;
                        for ( $i = 1; $i <= $data['PhotoNumber']; $i++) :
                            $img_uri = $this->remove_uri . '/photos/' . $data['PropertyID'] . '/' . $data['PropertyID'] . '_'.$i.'.jpg';
                            if( is_array(@getimagesize( $img_uri ) ) ):
                                $counter++;
                                ?>
                                <li>
                                    <img src="<?=$img_uri?>" style="height:300px;">
                                </li>
                            <?php endif;
                        endfor; ?>

                        <?php if( $counter == 0): ?>
                            <li>
                                <img src="<?=$this->plugin_uri . 'upload/house_holder.png'?>" style="height:300px;">
                            </li>
                        <?php endif; ?>
                    </ul>
                    <a href="#" class="jcarousel-control-prev prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next next">&rsaquo;</a>
                    <p class="jcarousel-pagination"></p>
                </div>
            </div>
        <?php else: ?>
            <div class="jcarousel-wrapper" style="width:<?=$img_w?>; height:<?=$map_img_h?>;">
                <div class="jcarousel">
                    <ul>
                        <?php
                        $counter = 0;
                        for ( $i = 1; $i <= $data['PhotoNumber']; $i++) :
                            $img_uri = $this->remove_uri . '/photos/' . $data['PropertyID'] . '/' . $data['PropertyID'] . '_'.$i.'.jpg';
                            if( is_array(@getimagesize( $img_uri ) ) ):
                                $counter++;
                                ?>
                                <li>
                                    <img src="<?=$img_uri?>" style="width:<?=$img_w?>; height:<?=$img_h?>;">
                                </li>
                            <?php endif; endfor; ?>

                        <?php if( $counter == 0): ?>
                            <li>
                                <img src="<?=$this->plugin_uri . 'upload/house_holder.png'?>" style="width:<?=$img_w?>; height:<?=$img_h?>;">
                            </li>
                        <?php endif; ?>
                    </ul>
                    <a href="#" class="jcarousel-control-prev prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next next">&rsaquo;</a>
                    <p class="jcarousel-pagination"></p>
                </div>
            </div>
        <?php endif; ?>


        <div id="map-overlay" class="google-maps" style='width:<?=$map_img_w?>; height:<?=$map_img_h?>;'>
            <iframe id="map" class="scrolloff" frameborder="0"  scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?php echo urlencode($data['Address']); ?>&output=embed&iwloc=near"></iframe>
        </div>
        <div class="house-details">
            <div class="table-row">
                <div class="table price-info">
                    <h1 class="text-left title">
                        <?php
                        if( $data['Sold'] == 'YES' ){
                            echo 'Past Listing';
                        }else {
                            if( !in_array($data['ListingType'], array('For sale', 'For sale or rent') ) ) {
                                list( $price, $term ) = explode( '/', $data['Price'] );
                                echo money_format( '%.0n', (double) $price );
                                if($term == 'Monthly') {
                                    echo '/mo.';
                                }else if(trim($term) == 'square feet' or empty($term)){
                                    echo '/sq.';
                                }
                            }
                            else {
                                echo money_format( '%.0n', (double) $data['Price'] );
                            }
                        }
                        ?>
                    </h1>
                    <span class="span_block address"><?=$data['Address']?></span>
                    <span class="span_block mls"><?='Listing ID: ' . $data['ListingID']?></span>
                </div>
            </div>
            <div class="table-row">
                <div class="table quick-view">
                    <div class="d-row">
                        <span class="span_block">Bathrooms: </span>
                        <span><?=$data['BathroomTotal']?></span>
                    </div>
                    <div class="d-row">
                        <span class="span_block">Bedrooms: </span>
                        <span><?=$data['BedroomsTotal']?></span>
                    </div>
                    <div class="d-row">
                        <span class="span_block">Property Type: </span>
                        <span><?=$data['PropertyType']?></span>
                    </div>
                    <div class="d-row">
                        <span class="span_block">Building Type: </span>
                        <span><?=$data['BuildingType']?></span>
                    </div>
                    <?php if( !empty($openHouseDate) ):  ?>
                    <div class="open-house">
                        <span class="span_block">Open House:</span>
                        <?php

                        if(isset($openHouseDate['StartDateTime'])) :
                            $StartDateTime = str_replace( '/', '-', $openHouseDate['StartDateTime'] );
                            $EndDateTime   = str_replace( '/', '-', $openHouseDate['EndDateTime'] );

?>
                            <div class="items">
                                <span class="time"><?=date('l M d', strtotime($StartDateTime))?> from <?=date('g:i a', strtotime($StartDateTime))?></span>
                                to <span class="time"><?=date('g:i a', strtotime( $EndDateTime ))?></span>
                            </div>
<?php
                        else:
                            foreach($openHouseDate as $k=>$item):
                                $StartDateTime = str_replace( '/', '-', $item['StartDateTime'] );
                                $EndDateTime   = str_replace( '/', '-', $item['EndDateTime'] );
                            ?>
                            <div class="items">
                                <span class="time"><?=date('l M d', strtotime($StartDateTime))?> from <?=date('g:i a', strtotime($StartDateTime))?></span>
                                to <span class="time"><?=date('g:i a', strtotime( $EndDateTime ))?></span>
                            </div>
                        <?php endforeach; endif; ?>

                        <?php endif; ?>
                        <?php if( isset($single['AlternateURL']) and isset($single['AlternateURL']['VideoLink']) ):  ?>
                                <a class="v-tour-link" target="_blank" href="<?=$single['AlternateURL']['VideoLink']?>">Virtual Tour</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="table-row">
                <div class="table intro">
                    <h4 class="text-left span_block">Property Description</h4>
                    <p class="details"><?php
                        $content = $data['PublicRemarks'];
                        $content = str_replace('**** EXTRAS ****', '</p>** EXTRAS **<p>', $content );
                        echo $content;
                        ?></p>
                </div>
            </div>

            <div class="table-row">
                <div class="table summary-house">
                    <h4 class="text-left span_block with-bg">Building Details</h4>
                    <div class="summary-house-wrapper">
                        <?php
                        foreach( $meta['Building'] as $cat => $value ):
                            if($cat != 'Rooms' and !empty($meta['Building'][$cat]) ):
                                ?>
                                <div class="d-row">
                                    <span class="span_block"><?=join(' ', splitAtUpperCase($cat))?></span>
                                    <span><?php
                                        $da = idx($meta['Building'], $cat, 0 );
                                        echo ( trim(strtolower($da))=='false' ? '' : $da );
                                        ?></span>
                                </div>
                            <?php endif; endforeach; ?>
                    </div>
                </div>
            </div>

            <?php if(isset($meta['Building']['Rooms']) ):?>
                <div class="table-row">
                    <h4 class="text-left span_block with-bg">Room Details</h4>
                    <table class="summary-room">
                        <tr>
                            <th>Level</th>
                            <th>Type</th>
                            <th>Dimensions</th>
                        </tr>
                        <?php foreach( $meta['Building']['Rooms']['Room'] as $k => $value ): ?>
                            <tr>
                                <td><?=$value['Level']?></td>
                                <td><?=$value['Type']?></td>
                                <td><?=(is_array($value['Dimension']) ? '' : $value['Dimension'])?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>

                </div>
            <?php endif; ?>
            <div class="table-row">
                <div class="table summary-house">
                    <h4 class="text-left span_block with-bg">Additional Information</h4>
                    <div class="summary-house-wrapper">
                        <div class="d-row">
                            <span class="span_block">Features</span>
                            <span><?=idx($meta, 'Features', 0)?></span>
                        </div>
                        <div class="d-row">
                            <span class="span_block">Ownership</span>
                            <span><?=idx($meta, 'OwnershipType', 0)?></span>
                        </div>
                        <div class="d-row">
                            <span class="span_block">Parking</span>
                            <ul class="parking">
                                <?php
                                if( isset($meta['ParkingSpaces']) and isset($meta['ParkingSpaces']['Parking']) ){
                                    foreach( $meta['ParkingSpaces']['Parking'] as $k => $value):
                                        if(is_array($value)){
                                            echo '<li>' . $value['Name'] . ' - ' . $value['Spaces'] . '</li>';
                                        }else {
                                            echo '<li>' . $k . ' - ' . $value . '</li>';
                                        }
                                    endforeach;
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="d-row">
                            <span class="span_block">Transaction</span>
                            <span><?=idx($meta, 'TransactionType', 0)?></span>
                        </div>
                        <?php if( isset($meta['Land']) ):?>
                            <div class="d-row">
                                <span class="span_block">Land</span>
                                <ul class="land">
                                    <?php foreach( $meta['Land'] as $k=>$v): if($v):?>
                                        <li><span class="sub-title"><?=join(' ',splitAtUpperCase($k))?>: </span> <?=(trim(strtolower($v))=='false'?'':$v)?></li>
                                    <?php endif; endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php //if( $show_agent == 1 ): ?>
            <span class="agent-office"><?php
                if(isset($meta['AgentDetails'][0])){
                    echo $meta['AgentDetails'][0]['Office']['Name'];
                }else{
                    echo $meta['AgentDetails']['Office']['Name'];
                }
                ?></span>
            <?php //endif;?>

            <div class="d-row text-center">
                <img src="<?php echo  plugin_dir_url( dirname( dirname( __FILE__ ) ) ); ?>public/img/REALTOR-MLS-logos.jpg" width="150" height="auto"><br>
                <span>The trademarks MLS®, REALTOR®, and the associated logos are owned or controlled by The Canadian Real Estate Association.  Used under license.</span>
            </div>
        </div>

    </div>

    <script>
        (function($) {
            $(function() {
                $('.jcarousel').jcarousel();

                $('.jcarousel-control-prev')
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .jcarouselControl({
                        target: '-=1'
                    });

                $('.jcarousel-control-next')
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .jcarouselControl({
                        target: '+=1'
                    });


                $('#map-overlay').on("mouseup",function(){
                    $('#map').addClass('scrolloff');
                }).on("mousedown",function(){
                    $('#map').removeClass('scrolloff');
                });

                $("#map").mouseleave(function () {
                    $('#map').addClass('scrolloff');

                });
                
            });
        })(jQuery);

    </script>
<?php wp_footer(); ?>




<?php
$content = simplexml_load_string(
    '<JS>'.$meta['AnalyticsClick'].'</JS>'
);
echo (string) $content;

$content = simplexml_load_string(
    '<JS>'.$meta['AnalyticsView'].'</JS>'
);
echo (string) $content;

/*

Array
(
    [@attributes] => Array
        (
            [ID] => 16109785
            [LastUpdated] => Fri, 04 Sep 2015 06:33:54 GMT
        )

    [ListingID] => 2092172
    [AgentDetails] => Array
        (
            [0] => Array
                (
                    [@attributes] => Array
                        (
                            [ID] => 1389318
                        )

                    [Name] => Paul Butler
                    [Office] => Array
                        (
                            [@attributes] => Array
                                (
                                    [ID] => 273066
                                )

                            [Name] => RE/MAX Aboutowne Realty Corp,; Brokerage
                            [Address] => Array
                                (
                                    [StreetAddress] => 1011 Upper Middle Road East
                                    [AddressLine1] => 1011 Upper Middle Road East
                                    [City] => Oakville
                                    [PostalCode] => L6J4L2
                                )

                            [Phones] => Array
                                (
                                    [Phone] => Array
                                        (
                                            [0] => (905) 842-8000
                                            [1] => (905) 842-8482
                                        )

                                )

                        )

                    [Designations] => Array
                        (
                            [Designation] => BROKER
                        )

                )

            [1] => Array
                (
                    [@attributes] => Array
                        (
                            [ID] => 1946760
                        )

                    [Name] => Heather Clark Smith
                    [Websites] => Array
                        (
                            [Website] => http://heatherclarksmith.com
                        )

                    [Office] => Array
                        (
                            [@attributes] => Array
                                (
                                    [ID] => 273066
                                )

                            [Name] => RE/MAX Aboutowne Realty Corp,; Brokerage
                            [Address] => Array
                                (
                                    [StreetAddress] => 1011 Upper Middle Road East
                                    [AddressLine1] => 1011 Upper Middle Road East
                                    [City] => Oakville
                                    [PostalCode] => L6J4L2
                                )

                            [Phones] => Array
                                (
                                    [Phone] => Array
                                        (
                                            [0] => (905) 842-8000
                                            [1] => (905) 842-8482
                                        )

                                )

                        )

                    [Designations] => Array
                        (
                            [Designation] => SALESPERSON
                        )

                )

        )

    [Board] => 15
    [Business] => Array
        (
        )

    [Building] => Array
        (
            [BathroomTotal] => 3
            [BedroomsTotal] => 4
            [BedroomsAboveGround] => 4
            [BedroomsBelowGround] => 0
            [Appliances] => Central Vacuum
            [BasementDevelopment] => Unfinished
            [BasementType] => Full (Unfinished)
            [ConstructionStyleAttachment] => Detached
            [CoolingType] => Central air conditioning
            [ExteriorFinish] => Brick, Stone
            [FireplaceFuel] => Gas
            [FireplacePresent] => True
            [FireplaceType] => Unknown
            [HalfBathTotal] => 1
            [HeatingFuel] => Natural gas
            [HeatingType] => Forced air
            [Rooms] => Array
                (
                    [Room] => Array
                        (
                            [0] => Array
                                (
                                    [Type] => Great room
                                    [Width] => 12 ft
                                    [Length] => 15 ft
                                    [Level] => Main level
                                    [Dimension] => 15 ft x 12 ft
                                )

                            [1] => Array
                                (
                                    [Type] => Breakfast
                                    [Width] => 9 ft ,8 in
                                    [Length] => 14 ft ,2 in
                                    [Level] => Main level
                                    [Dimension] => 14 ft ,2 in x 9 ft ,8 in
                                )

                            [2] => Array
                                (
                                    [Type] => Dining room
                                    [Width] => 11 ft ,6 in
                                    [Length] => 15 ft
                                    [Level] => Main level
                                    [Dimension] => 15 ft x 11 ft ,6 in
                                )

                            [3] => Array
                                (
                                    [Type] => Bedroom 2
                                    [Width] => 10 ft
                                    [Length] => 10 ft ,6 in
                                    [Level] => Second level
                                    [Dimension] => 10 ft ,6 in x 10 ft
                                )

                            [4] => Array
                                (
                                    [Type] => Kitchen
                                    [Width] => 10 ft ,6 in
                                    [Length] => 14 ft
                                    [Level] => Main level
                                    [Dimension] => 14 ft x 10 ft ,6 in
                                )

                            [5] => Array
                                (
                                    [Type] => Master bedroom
                                    [Width] => 12 ft ,6 in
                                    [Length] => 15 ft
                                    [Level] => Second level
                                    [Dimension] => 15 ft x 12 ft ,6 in
                                )

                            [6] => Array
                                (
                                    [Type] => Bedroom 3
                                    [Width] => 11 ft ,2 in
                                    [Length] => 11 ft ,6 in
                                    [Level] => Second level
                                    [Dimension] => 11 ft ,6 in x 11 ft ,2 in
                                )

                            [7] => Array
                                (
                                    [Type] => Bedroom 4
                                    [Width] => 10 ft
                                    [Length] => 10 ft ,6 in
                                    [Level] => Second level
                                    [Dimension] => 10 ft ,6 in x 10 ft
                                )

                            [8] => Array
                                (
                                    [Type] => Laundry room
                                    [Width] => 6 ft ,3 in
                                    [Length] => 7 ft ,9 in
                                    [Level] => Second level
                                    [Dimension] => 7 ft ,9 in x 6 ft ,3 in
                                )

                            [9] => Array
                                (
                                    [Type] => Utility room
                                    [Width] => Array
                                        (
                                        )

                                    [Length] => Array
                                        (
                                        )

                                    [Level] => Main level
                                    [Dimension] => Array
                                        (
                                        )

                                )

                            [10] => Array
                                (
                                    [Type] => 5pc Bathroom
                                    [Width] => Array
                                        (
                                        )

                                    [Length] => Array
                                        (
                                        )

                                    [Level] => Second level
                                    [Dimension] => Array
                                        (
                                        )

                                )

                            [11] => Array
                                (
                                    [Type] => 4pc Bathroom
                                    [Width] => Array
                                        (
                                        )

                                    [Length] => Array
                                        (
                                        )

                                    [Level] => Second level
                                    [Dimension] => Array
                                        (
                                        )

                                )

                            [12] => Array
                                (
                                    [Type] => 2pc Bathroom
                                    [Width] => Array
                                        (
                                        )

                                    [Length] => Array
                                        (
                                        )

                                    [Level] => Main level
                                    [Dimension] => Array
                                        (
                                        )

                                )

                        )

                )

            [StoriesTotal] => 2
            [Type] => House
            [UtilityWater] => Municipal water
        )

    [Land] => Array
        (
            [SizeTotalText] => 38' x 95' x 71' x 89'  Irr Pie|under 1/4 acre
            [Acreage] => false
            [Sewer] => Municipal sewage system
            [SizeIrregular] => 38' x 95' x 71' x 89'  Irr Pie
        )

    [Address] => Array
        (
            [StreetAddress] => 47 SIXTEEN MILE Drive
            [StreetNumber] => 47
            [StreetName] => SIXTEEN MILE
            [StreetSuffix] => Drive
            [City] => Oakville
            [Province] => Ontario
            [PostalCode] => L6M0W3
            [Country] => Canada
            [CommunityName] => GO Glenorchy
            [lat] =>
            [lng] =>
            [formatted] =>
        )

    [AlternateURL] => Array
        (
            [VideoLink] => http://virtualviewing.ca/virtualtour/index.php/47-sixteen-mile-dr-oakville-unbranded/
        )

    [Features] => Bathroom adjoining master bedroom
    [LeasePerTime] => Monthly
    [OwnershipType] => Freehold
    [ParkingSpaces] => Array
        (
            [Parking] => Array
                (
                    [0] => Array
                        (
                            [Name] => Garage
                            [Spaces] => 2
                        )

                    [1] => Array
                        (
                            [Name] => Open
                            [Spaces] => 2
                        )

                )

        )

    [Photo] => Array
        (
            [PropertyPhoto] => Array
                (
                    [0] => Array
                        (
                            [SequenceId] => 1
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [1] => Array
                        (
                            [SequenceId] => 2
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [2] => Array
                        (
                            [SequenceId] => 3
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [3] => Array
                        (
                            [SequenceId] => 4
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [4] => Array
                        (
                            [SequenceId] => 5
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [5] => Array
                        (
                            [SequenceId] => 6
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [6] => Array
                        (
                            [SequenceId] => 7
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [7] => Array
                        (
                            [SequenceId] => 8
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [8] => Array
                        (
                            [SequenceId] => 9
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [9] => Array
                        (
                            [SequenceId] => 10
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [10] => Array
                        (
                            [SequenceId] => 11
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [11] => Array
                        (
                            [SequenceId] => 12
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [12] => Array
                        (
                            [SequenceId] => 13
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [13] => Array
                        (
                            [SequenceId] => 14
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [14] => Array
                        (
                            [SequenceId] => 15
                            [LastUpdated] => 04/09/2015 1:33:53 AM
                        )

                    [15] => Array
                        (
                            [SequenceId] => 16
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [16] => Array
                        (
                            [SequenceId] => 17
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [17] => Array
                        (
                            [SequenceId] => 18
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [18] => Array
                        (
                            [SequenceId] => 19
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [19] => Array
                        (
                            [SequenceId] => 20
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [20] => Array
                        (
                            [SequenceId] => 21
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [21] => Array
                        (
                            [SequenceId] => 22
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [22] => Array
                        (
                            [SequenceId] => 23
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [23] => Array
                        (
                            [SequenceId] => 24
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                    [24] => Array
                        (
                            [SequenceId] => 25
                            [LastUpdated] => 04/09/2015 1:33:54 AM
                        )

                )

        )

    [Price] => 989900.00
    [PropertyType] => Single Family
    [PublicRemarks] => Nestled in the heart of Oakville in the desirable community of The Preserve.  This stunning stucco front exterior Mattamy built, ""Alder"" model, 4 bedroom family home has been upgraded top to bottom to the highest level.  Approximately 2500 sq ft home on a premium oversized fenced pool sized lot 71 feet wide at rear, across the street from ""Isaac"" Park.  The Double Height Foyer & Grand Staircase of squared white pickets and dark hardwood treds/risers, greets you. California Shutters. Potlights, Crown Mouldings and Granite countertops. S/S Appliances including Double door/2 drawer Fridge, Built-in Stove, Range Hood, Microwave. Kitchen is complete with Backsplash, Pantry, Glass Front Cabinets that flank the Stove & sliding glass door walkout to the premium yard. Enter the 2nd Floor Master Retreat, through the alcove and architectural rotunda; his and hers closets; Ensuite with raised counter height & porcelain tiled flooring,Hardwood floor upgraded from front door through to the Family Room, and 2nd floor landing. porcelain tile, glass shower, and soaker tub.  Three bright additional good size bedrooms, 1 w vaulted ceiling; laundry room and main bathroom complete the 2nd floor. Please note the laundry is on the second floor and there is a R/I  bath in the lower level.  As well the garage door can be controlled via a smartphone. Please note the 9' ceilings on both the main and upper floor and the upgraded large windows on the second level.  Located close to new Oakville Hospital and GO train.
    [TransactionType] => For sale
    [UtilitiesAvailable] => Array
        (
        )

    [ZoningDescription] => ResID
    [AnalyticsClick] => <![CDATA[<script type='text/javascript'>
 function redirect() {
var event = new RlEvent('crealocation');
event.viewedProperty(16109785,9453,[1389318,1946760],[273066,273066],[1736204,1736204],[0,0]);
event.set('event', 'type', 'click');
event.record();
setTimeout("window.location = 'http://www.realtor.ca/PropertyDetails.aspx?PropertyId=16109785'",500);}
</script>]]>
    [AnalyticsView] => <![CDATA[<script src='http://analytics.crea.ca/crearl.js' type='text/javascript'></script>
<script type='text/javascript'>
var event = new RlEvent('crealocation');
event.viewedProperty(16109785,9453,[1389318,1946760],[273066,273066],[1736204,1736204],[0,0]);
event.set('event', 'type', 'view');
event.record();
</script>]]>
    [MoreInformationLink] => http://www.realtor.ca/PropertyDetails.aspx?PropertyId=16109785
    [task_id] => 55ea4e9e741f8400090080ad
)



 */