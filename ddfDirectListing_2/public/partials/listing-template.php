<?php
    $remove_uri =  $this->remove_uri;
    $plugin_uri =  $this->plugin_uri;
    $last_id    =  0;

    $sold = false;
    if(isset($short_code)) {
        if ( ( isset( $short_code['listing_type'] )
               and $short_code['listing_type'] == 'past'
             )
             or
             $short_code['type'] == 'agent_past_listings'
        ) {
            $sold = true;
        }
    }
    foreach ( $listing as $key => $value):
     if($value['Sold'] == 'YES'){
         $sold = true;
     }
?>
<div class="wrapper">
    <div class="list" id="<?=$value['id']?>">
        <div class="image-wrapper img-responsive ">
            <?php
                if( getHowLongAgo( $value['LastUpdated'] ) < $this->options_display['new_listing_days'] ): ?>
                <img width="80" height="80" class="new-label" src="<?php
                    echo  $plugin_uri . 'upload/label/' . $this->options_display['label_style'] . '.png';
                ?>">
            <?php endif; ?>
            <a href="/property/<?php echo $value['PropertyID']; ?>" class="img" style="background-image: url('<?php
                if( $value['PhotoNumber'] > 0 ){
                    echo $remove_uri . '/photos/' . $value['PropertyID'] . '/' . $value['PropertyID'] . '_1.jpg';
                }else{
                    echo $plugin_uri . 'upload/house_holder.png';
                }
                ?>');">
            </a>

        </div>
        <div class="sum-info img-responsive">

            <h3 class="price"><?php
                if( $sold == true){
                    echo '<span class="sold"><a href="/property/'.$value['PropertyID'].'">Past Listing</a></span>';
                }else {

                    echo '<a href="/property/'.$value['PropertyID'].'">';
                    if( !in_array($value['ListingType'], array('For sale', 'For sale or rent') ) ) {
                        list( $price, $term ) = explode( '/', $value['Price'] );
                        echo money_format( '%.0n', (double) $price );
                        if($term == 'Monthly') {
                            echo '/mo.';
                        }else if(trim($term) == 'square feet' or empty($term)){
                            echo '/sq.';
                        }

                    } else {
                        echo money_format( '%.0n', (double) $value['Price'] );
                    }
                    echo '</a>';
                }
                ?></h3>

            <ul class="room-details">
                <li class="bedroom icon">
                    <span class="badge"><?=$value['BedroomsTotal']?></span>
                </li>
                <li class="bathroom icon">
                    <span class="badge"><?=$value['BathroomTotal']?></span>
                </li>
            </ul>

            <h5 class="address">
                <?php
                $address = str_replace('Canada', '', $value['Address']);
                $address = str_replace('canada', '', $address);
                echo $address;
                ?>
            </h5>
            <?php if( !wp_is_mobile() ): ?>
                <span class="intro"><?php
                    echo substr($value['PublicRemarks'], 0, 400).'... <a href="/property/'.$value['PropertyID'].'" class="read-more"> Read More </a>';
                    ?></span>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php endforeach; ?>