<?php
	global $blog_id;

	$listing = std_to_array( json_decode( $data ) );
	$cookie_name = 'ddf_direct_listing_cookie_'.$blog_id;
	$overlay = '';
	$my_option = '';
	if( empty($_COOKIE[$cookie_name]) and !is_front_page()) {
		$overlay = 'overlay-open';
	}
?>
<div class="ddf-direct-listing" data-cookie="<?php echo $cookie_name; ?>">
	<?php if(!empty($overlay)):?>
		<div class="terms <?=$overlay?>" id="ddf-terms">
			<div class="term-details">
				<div class="term-inner">
					<div class="term-header">Terms of Use</div>
					<div class="term-body"><?php
						echo $this->options_display['aggrement'];
						?></div>
					<div class="term-footer">
						<a class="term-action declien">Decline</a>
						<a class="term-action accept">Accept</a>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="listings">
        <div class="listing-wrapper">
        <?php include ('listing-template.php'); ?>
		<?php if(count($listing) >= $this->options_display['listing_limit'] and idx($short_code, 'show_all', "false") == "false" ) :?>
        <div class="more-result"></div>
        <div class="view-more">
            <button class="btn btn-danger more-btn"
                    role="more"
                    data-query="<?php
	                    if( isset($_POST['search']) and $_POST['search'] == 'global'){
		                    echo urlencode(json_encode($_POST));
	                    }

                        if( isset($pass_query) and !empty($pass_query)){
	                        echo urlencode(json_encode(array('meta'=>$pass_query)));
                        }
                    ?>"
                    data-type="<?=$short_code['type']?>"
                    data-offset="<?=$this->options_display['listing_limit']?>">View More <i class="fa fa-spinner fa-spin"></i>
            </button>
        </div>
		<?php endif;?>
    <div class="clear"></div>
    </div>
	</div>
</div>