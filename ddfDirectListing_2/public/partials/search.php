<div class="ddf-direct-listing">
	<div class="listings">
		<div class="search-bar">
			<form id="global_search" method="POST" action="">
				<div class="search-input">
					<div class="input-mask">
						<span class="title">Search All Listings</span>
					</div>
					<input type="text" id="keyword" class="input-lg" name="keyword">
				</div>
				<div class="search-option primary">
					<div class="form-control">
						<select id="BuildingType" name="BuildingType">
							<option value="0">Property Type</option>
							<option value="Apartment">Apartment</option>
							<option value="Commercial Apartment">Commercial Apartment</option>
							<option value="Commercial Mix">Commercial Mix</option>
							<option value="Duplex">Duplex</option>
							<option value="Fourplex">Fourplex</option>
							<option value="House">House</option>
							<option value="Manufacturing">Manufacturing</option>
							<option value="Mobile Home">Mobile Home</option>
							<option value="Modular">Modular</option>
							<option value="Multi-Family">Multi-Family</option>
							<option value="Multi-Tenant Industrial">Multi-Tenant Industrial</option>
							<option value="Offices">Offices</option>
							<option value="Parking">Parking</option>
							<option value="Recreational">Recreational</option>
							<option value="Residential Commercial Mix">Residential Commercial Mix</option>
							<option value="Retail">Retail</option>
							<option value="Row / Townhouse">Row / Townhouse</option>
							<option value="Special Purpose">Special Purpose</option>
							<option value="Triplex">Triplex</option>
							<option value="Warehouse">Warehouse</option>
						</select>
					</div>
					<div class="form-control">
						<select id="TransactionType" name="TransactionType">
							<option value="For sale" selected>For sale</option>
							<option value="For rent">For rent</option>
						</select>
					</div>
					<div class="form-control">
						<select id="BedRange" name="BedRange">
							<option value="0" selected>Bedrooms</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="9">5+</option>
						</select>
					</div>
					<div class="form-control">
						<select id="BathRange" name="BathRange">
							<option value="0" selected>Bathrooms</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="9">5+</option>
						</select>
					</div>
					<div class="form-control">
						<select id="PriceMin" name="PriceMin">
							<option value="" selected>Lower Price Limit</option>
							<option value="0">$0</option>
							<option value="25000">$25,000</option>
							<option value="50000">$50,000</option>
							<option value="75000">$75,000</option>

							<option value="100000">$100,000</option>
							<option value="125000">$125,000</option>
							<option value="150000">$150,000</option>
							<option value="175000">$175,000</option>

							<option value="200000">$200,000</option>
							<option value="225000">$225,000</option>
							<option value="250000">$250,000</option>
							<option value="275000">$275,000</option>

							<option value="300000">$300,000</option>
							<option value="325000">$325,000</option>
							<option value="350000">$350,000</option>
							<option value="375000">$375,000</option>

							<option value="400000">$400,000</option>
							<option value="450000">$450,000</option>
							<option value="475000">$475,000</option>

							<option value="500000">$500,000</option>
							<option value="550000">$550,000</option>
							<option value="575000">$575,000</option>

							<option value="600000">$600,000</option>
							<option value="650000">$650,000</option>
							<option value="675000">$675,000</option>

							<option value="700000">$700,000</option>
							<option value="750000">$750,000</option>
							<option value="775000">$775,000</option>

							<option value="800000">$800,000</option>
							<option value="850000">$850,000</option>

							<option value="900000">$900,000</option>
							<option value="950000">$950,000</option>

							<option value="1000000">$1,000,000</option>
							<option value="1100000">$1,100,000</option>
							<option value="1200000">$1,200,000</option>
							<option value="1300000">$1,300,000</option>
							<option value="1400000">$1,400,000</option>
							<option value="1500000">$1,500,000</option>
							<option value="1600000">$1,600,000</option>
							<option value="1700000">$1,700,000</option>
							<option value="1800000">$1,800,000</option>
							<option value="1900000">$1,900,000</option>

							<option value="2000000">$2,000,000</option>
							<option value="2500000">$2,500,000</option>
							<option value="3000000">$3,000,000</option>
							<option value="3000000">$3,000,000</option>
							<option value="4000000">$4,000,000</option>
							<option value="5000000">$5,000,000</option>
							<option value="7500000">$7,500,000</option>
							<option value="10000000">$10,000,000</option>
						</select>
					</div>
					<div class="form-control">
						<select id="PriceMax" name="PriceMax">
							<option value="">Upper Price Limit</option>
							<option value="-1">Unlimited</option>
							<option value="25000">$25,000</option>
							<option value="50000">$50,000</option>
							<option value="75000">$75,000</option>

							<option value="100000">$100,000</option>
							<option value="125000">$125,000</option>
							<option value="150000">$150,000</option>
							<option value="175000">$175,000</option>

							<option value="200000">$200,000</option>
							<option value="225000">$225,000</option>
							<option value="250000">$250,000</option>
							<option value="275000">$275,000</option>

							<option value="300000">$300,000</option>
							<option value="325000">$325,000</option>
							<option value="350000">$350,000</option>
							<option value="375000">$375,000</option>

							<option value="400000">$400,000</option>
							<option value="450000">$450,000</option>
							<option value="475000">$475,000</option>

							<option value="500000">$500,000</option>
							<option value="550000">$550,000</option>
							<option value="575000">$575,000</option>

							<option value="600000">$600,000</option>
							<option value="650000">$650,000</option>
							<option value="675000">$675,000</option>

							<option value="700000">$700,000</option>
							<option value="750000">$750,000</option>
							<option value="775000">$775,000</option>

							<option value="800000">$800,000</option>
							<option value="850000">$850,000</option>

							<option value="900000">$900,000</option>
							<option value="950000">$950,000</option>

							<option value="1000000">$1,000,000</option>
							<option value="1100000">$1,100,000</option>
							<option value="1200000">$1,200,000</option>
							<option value="1300000">$1,300,000</option>
							<option value="1400000">$1,400,000</option>
							<option value="1500000">$1,500,000</option>
							<option value="1600000">$1,600,000</option>
							<option value="1700000">$1,700,000</option>
							<option value="1800000">$1,800,000</option>
							<option value="1900000">$1,900,000</option>

							<option value="2000000">$2,000,000</option>
							<option value="2500000">$2,500,000</option>
							<option value="3000000">$3,000,000</option>
							<option value="3000000">$3,000,000</option>
							<option value="4000000">$4,000,000</option>
							<option value="5000000">$5,000,000</option>
							<option value="7500000">$7,500,000</option>
							<option value="10000000">$10,000,000</option>
						</select>
					</div>
					<div class="form-control open-house">
						<input id="OpenHouse" name="OpenHouse" class="" type="checkbox">
						<span class="fltr-openhouse">Open Houses</span>
					</div>
					<div class="form-control text-center">
						<input type="submit" class="submit" name="submit" value="Search" />
					</div>
				</div>
				<input type="hidden" name="search" value="global">
			</form>
		</div>
	</div>
</div>
<div class="clear-both"></div>