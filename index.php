<?php
class Iron_Uploader{

	protected $iron_workers;
	protected $allowed_ips;
	protected $user_ip;

	public function __construct() {

		#$this->user_ip		=  $_SERVER['ADD_REMOTE'];
		$this->iron_workers	=  $GLOBALS['worker_map'];

		return $this;
	}
	/**
	 * Upload initialized workers to repo
	 *
	 */
	public function uploader(){
		try {
			$counter = 0;
			foreach ( $this->iron_workers as $key => $value){

				echo ($counter+1).'. ';

				if( is_file( dirname(__FILE__)."/workers/".$key."/index.php")){

					ob_start();
					require_once 'workers/'.$key.'/index.php';
					ob_end_flush() ;

				}  else {
					echo "<b>workers/".$value."/index.php</b> not exists.<b>";
				}
				$counter++;
			}
		} catch (Exception $exc) {

			echo 'Error: <br>' .$exc->getTraceAsString();

		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>DDF Workers Tool</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
</head>
<body>
<div class="container">
	<h1 class="text-center">DDF Worker Tool</h1>
	<h2>Action</h2>
	<a class="btn btn-primary" href="/?action=upload">Upload Workers</a>
	<h2>Result</h2>
	<div class="jumbotron">
		<?php
		if( isset($_GET['action']) ){
			require_once "phar://iron_worker.phar";
			require_once "vars.php";

			# DS value for global uploading
			# Need redefined it for local uploading
			define('DS', '');
			if( $_GET['action'] == "upload"){
				$action = (new Iron_Uploader())->uploader();
			}
			echo '<br><br>Success.';
		}
		?>
	</div>
</div>
</body>
</html>