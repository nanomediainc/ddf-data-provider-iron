<?php

if( !isset($_POST['id']) or $_POST['token'] != '8%3+^=9||18:C38+8%3+^=979^818:C3V') {
	exit(1);
}

require("libs/PHRets_CREA.php");
$debug = true;

function debug_log($file_name, $content) {
	global $debug;

	if($debug) {
		$log_file    = dirname( __FILE__ ) . '/log/' . $file_name;
		file_put_contents( $log_file, print_r( $content, 1 ), FILE_APPEND);
	}
}

debug_log('post.log', $_POST);

$RETS = new PHRets();
$RETSURL = "http://data.crea.ca/Login.svc/Login";
$RETSUsername = "sQ5oAmL0idpecN2MtWqDaTq0";
$RETSPassword = "dUxnQoPo7Z4KLTOa5iK0hRsT";
$RETS->Connect($RETSURL, $RETSUsername, $RETSPassword);
$RETS->AddHeader("RETS-Version", "RETS/1.7.2");
$RETS->AddHeader('Accept', '/');
$RETS->SetParam('compression_enabled', true);
$RETS_PhotoSize = "LargePhoto";
$RETS_LimitPerQuery = 1;

$listingID = $_POST['id'];
$results = $RETS->SearchQuery(
	"Property",
	"Property",
	"(ID={$listingID})",
	array( "Format" => "STANDARD-XML", "Count" => 1, "Culture"=>"en-CA", "Limit"=>1)
);

if( current( $results['Count'] ) < 1 ){
	die(0);
}

# Testing get listing
$property = current($results['Properties']);
echo json_encode($property);

$RETS->Disconnect();

