<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-08-03
 * Time: 11:42 AM
 */

require_once('../wp-config.php');
$debug = true;

function debug_log($file_name, $content) {

	global $debug;

	if($debug) {
		$log_file    = dirname( __FILE__ ) . '/log/' . $file_name;
		file_put_contents( $log_file, print_r( $content, 1 ), FILE_APPEND);
	}
}

function std_to_array( $std ){
	return json_decode( json_encode($std), true );
}

function save_data(){

	global $wpdb;

	$listing = json_decode(  urldecode( $_POST['data'] ) );
	$listing = std_to_array($listing);

	debug_log('data.log', $listing);

	$PhotoNumber = (isset($listing['Photo']) and isset($listing['Photo']['PropertyPhoto'])) ? count($listing['Photo']['PropertyPhoto']) : 0;
	$Price = 0;
	$result = 0;

	if( isset($listing['Price']) ){
		$Price = $listing['Price'];
	}else if( isset($listing['Lease']) and isset($listing['LeasePerTime']) ){
		$Price = $listing['Lease'].'/'.$listing['LeasePerTime'];
	}else if( isset($listing['Lease']) and isset($listing['LeasePerUnit']) ){
		$Price = $listing['Lease'].'/'.$listing['LeasePerUnit'];
	}

	$AgentId = '';
	# Prepare the agent id(s)
	if(isset($listing['AgentDetails']['@attributes'])){
		$AgentId = $listing['AgentDetails']["@attributes"]["ID"];
	}else{
		foreach ( $listing['AgentDetails'] as $k => $v ) {
			if ( $k > 0 ) {
				$AgentId .= ',';
			}
			$AgentId .= @$listing['AgentDetails'][$k]['@attributes']["ID"];
		}
	}

	$Address = $listing['Address']['formatted'];
	if( isset($listing['Address']) ){
		$Address = (!empty($listing['Address']['StreetAddress']) ? $listing['Address']['StreetAddress'] : @$listing['Address']['AddressLine1'] );
		$Address .= ' ' . $listing['Address']['City'];
		$Address .= ' ' . $listing['Address']['Province'];
		$Address .= ' ' . $listing['Address']['PostalCode'];
		$Address .= ' ' . $listing['Address']['Country'];
	}

	$OpenHouse_Start = 0;
	$OpenHouse_End = 0;
	$OpenHouse  = 0;

	if(isset($listing['OpenHouse'])){
		if( isset($listing['OpenHouse']['Event'][0]) ){
			foreach( $listing['OpenHouse']['Event'] as $k => $date){
				$open_time = 	strtotime( str_replace( "/", "-", $listing['OpenHouse']['Event'][$k]['StartDateTime'] ) );
				$end_time  = 	strtotime( str_replace( "/", "-", $listing['OpenHouse']['Event'][$k]['EndDateTime'] ) );
				if( $end_time > time() ) {
					$OpenHouse_Start = date( 'Y-m-d H:i:s', $open_time );
					$OpenHouse_End   = date( 'Y-m-d H:i:s', $end_time );
					$OpenHouse  = 1;
					break;
				}
			}
		}else {
			$open_time = 	strtotime( str_replace( "/", "-", $listing['OpenHouse']['Event']['StartDateTime'] ) );
			$end_time  = 	strtotime( str_replace( "/", "-", $listing['OpenHouse']['Event']['EndDateTime'] ) );

			if( $end_time > time() ) {
				$OpenHouse_Start = date( 'Y-m-d H:i:s', $open_time );
				$OpenHouse_End   = date( 'Y-m-d H:i:s', $end_time );
				$OpenHouse       = 1;
			}
		}

	}
	debug_log('start.log', $OpenHouse_Start );

	$PostalCode = '';
	if(isset($listing['Address']['PostalCode'])){
		$PostalCode = ( strlen($listing['Address']['PostalCode']) > 3 ) ? substr($listing['Address']['PostalCode'], 0, -3) :$listing['Address']['PostalCode'];
	}

	$OfficeName = $listing['AgentDetails']['Office']['Name'];
	$OfficeID = $listing['AgentDetails']['Office']['@attributes']['ID'];


	if( key($listing['AgentDetails']) != '@attributes'){
		debug_log('list.log', 'innnnnnn' );
		$OfficeID = $listing['AgentDetails'][0]['Office']['@attributes']['ID'];
		$OfficeName = $listing['AgentDetails'][0]['Office']['Name'];
	}


	debug_log('list.log', $listing['AgentDetails']);
	debug_log('list.log', $listing['AgentDetails']['Office']['@attributes']['ID'] .' | '.$listing['AgentDetails']['Office']['Name'] );

	$info = array(    'PropertyID'    => $listing['@attributes']['ID'], //%d
	                  'ListingID'     => $listing['ListingID'], //%s
	                  'AgentID'       => $AgentId, //%s
	                  'OfficeID'      => $OfficeID, //%s
	                  'OfficeNAME'    => $OfficeName, //%s
	                  'OpenHouse'     => $OpenHouse,//%d
	                  'OpenHouse_Start' => $OpenHouse_Start,//%s
	                  'OpenHouse_End'   => $OpenHouse_End,//%s
	                  'ListingType'   => $listing['TransactionType'],//%s
	                  'PhotoNumber'   => $PhotoNumber,//%d
	                  'latitude'      => $listing['Address']['lat'],//%s
	                  'longitude'     => $listing['Address']['lng'],//%s
	                  'PostalCode'    => $PostalCode,//%s
	                  'City'          => $listing['Address']['City'],//%s
	                  'Address'       => $Address,//%s

					  'BathroomTotal' => $listing['Building']['BathroomTotal'],//%d
					  'BedroomsTotal' => $listing['Building']['BedroomsTotal'],//%d
					  'BuildingType'  => $listing['Building']['Type'],//%s
					  'Price'         => $Price,//%s
					  'PublicRemarks' => $listing['PublicRemarks'],//%s
					  'PropertyType'  => $listing['PropertyType'],//%s


	                  'LastUpdated'   => gmdate( 'Y-m-d H:i:s', strtotime($listing['@attributes']['LastUpdated'])),//%s
	                  'LastCronTime'  => gmdate( 'Y-m-d H:i:s', strtotime('today')),//%s

					  'Task_ID'       => (isset($listing['task_id']) ? $listing['task_id'] : 0),//%s

	);


	debug_log('insert.log', $info);
	$wpdb->flush();
	$res = $wpdb->replace( 'ddf_listing', $info, array(
		'%d', '%s', '%s', '%s', '%s'
		, '%d', '%s', '%s', '%s'
		, '%d', '%s', '%s', '%s', '%s', '%s'
		, '%d', '%d'
		, '%s', '%s', '%s', '%s', '%s', '%s', '%s' )
	);

	debug_log('last_error.log', $wpdb->last_error);

	if(!$res){
		return $res;
	}

	$counter = $wpdb->get_var( "SELECT COUNT(*) FROM ddf_listing_meta WHERE meta_key='raw' AND PropertyID=".(int)$listing['@attributes']['ID'] );
	debug_log('last_error.log', $wpdb->last_error);

	$res = 0;
	if($counter > 0 ){

		$wpdb->update( 'ddf_listing_meta',
			array(
				'meta_value' => json_encode( $listing ),
			),
			array(
				'PropertyID'     => $listing['@attributes']['ID'],
		        'meta_key'       => 'raw',
			),
			array( '%s'),
			array( '%d', '%s')
		);
	}else {

		$wpdb->insert( 'ddf_listing_meta', array(
			'PropertyID'     => $listing['@attributes']['ID'],
			'meta_key'       => 'raw',
			'meta_id'        => 'post_data',
			'meta_value'     => json_encode( $listing ),
		), array( '%d', '%s', '%s', '%s' ) );
	}

	debug_log('last_error.log', $wpdb->last_error);
	if($wpdb->last_error) {
		$res = 1;
	}
	debug_log('last_error.log', 'Result: '.$res);

	return $res;
}

if(!isset($_POST['data']) or empty($_POST['data']) or $_POST['token'] != '+T4179^818:C3Vp9+8%3+^=9||=2kJ') {
	exit(1);
}

try {
	debug_log('data_raw.log',$_POST);
	echo save_data();

}catch (Exception $e){
	file_put_contents( dirname( __FILE__ ) . '/data_exception_'.date('Y-m-d').'.log', $e->getMessage(), FILE_APPEND );
	exit( $e->getMessage() );

}