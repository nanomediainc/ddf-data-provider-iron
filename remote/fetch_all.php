<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-08-03
 * Time: 11:42 AM
 */

require_once('../wp-config.php');
$debug = true;

class DB_Action{

	private $db_name;
	private $db_user;
	private $db_pass;
	private $db_host;

	function __construct(){
		# Staging:
		#$this->db_name = 'snapshot_realtechpro';
		#$this->db_user = 'realtechpro';
		#$this->db_pass = 'GYzvK7b87kflC6o79bxa';
		#$this->db_host = '127.0.0.1';

		# Live:
		$this->db_name = 'wp_realtechpro';
		$this->db_user = 'realtechpro';
		$this->db_pass = 'GYzvK7b87kflC6o79bxa';
		$this->db_host = '127.0.0.1';
	}

	private function my_query($action, $data=array(), $custom_db=false){

		global $wpdb;

		$db = $wpdb;
		$result = 0;

		if($custom_db){
			$db = new wpdb( $this->db_user, $this->db_pass, $this->db_name, $this->db_host );
		}

		switch ( $action ){

			case 'get_agent_listings':
				$result = $db->get_results( "SELECT `PropertyID` FROM `ddf_listing` WHERE `AgentID` IN ('".$data['agent_id']."')", ARRAY_A );
				break;

			case 'get_agents':
				$result = $db->get_results( "SELECT `meta_value` FROM `wp_postmeta` WHERE `meta_key` = 'Agent_Key'", ARRAY_A );
				debug_log('test_'.$action.'_db.log', $db);
				debug_log('test_'.$action.'.log', $result);
				break;
			case 'get_agents_details':
				$result = $db->get_results( "SELECT m.`meta_value`, p.`post_title` FROM `wp_postmeta` AS m LEFT JOIN `wp_posts` AS p ON  p.`ID`= m.`post_id` WHERE m.`meta_key` = 'Agent_Key' ORDER BY `post_title` ASC", ARRAY_A );
				break;
			case 'sold_listing':
				$result = $db->query($db->prepare(
					"UPDATE `ddf_listing` ".
					"SET `Sold` = 'YES', `Task_ID` = CONCAT(`Task_ID`,',', %s) ".
					"WHERE `PropertyID` = %d"
					,$data['TaskID'], $data['PropertyID']
				));
				break;

			case 'mark_paid_agent':
				$result = $db->update( 'ddf_listing',
						array(
							'PaidAgent' => 'YES',
						),
						array(
							'PropertyID' => $data['PropertyID']
						),
						array( '%s'),
						array( '%d')
				);
				break;
			case 'get_total_listings':
				$sql = "SELECT `PropertyID` AS 'id' FROM `ddf_listing`";
				#$sql = "SELECT `PropertyID` AS 'id' FROM `ddf_listing` WHERE `Sold`='No' ";
				#$sql = "SELECT `PropertyID` AS 'id' FROM `ddf_listing` WHERE `OpenHouse` = 1 AND `Sold` = 'NO'";
				$result = $db->get_results( $sql, ARRAY_A );
				break;
			case 'get_active_listings':
				$sql = "SELECT `PropertyID` AS 'id' FROM `ddf_listing` WHERE `Sold`='No' ";
				$result = $db->get_results( $sql, ARRAY_A );
				break;

			case 'get_total_listings_counts':
				$result = $db->get_var( "SELECT count(*) FROM `ddf_listing` WHERE `Sold`='No' " );
				break;
			case 'get_group_listings':
				$result = $db->get_results( "SELECT `PropertyID` FROM `ddf_listing` LIMIT 5 OFFSET ".$data['offset']);
				break;

			case 'get_paid_agent_listings':
				$result = $db->get_results( "SELECT COUNT(`PropertyID`) AS 'total' FROM `ddf_listing` WHERE `PaidAgent` ='YES'", ARRAY_A );
				break;

			case 'get_paid_agent_sold_listings':
				$result = $db->get_results( "SELECT COUNT(`PropertyID`) AS 'total'  FROM `ddf_listing` WHERE `PaidAgent` ='YES' AND `Sold`='YES'", ARRAY_A );
				break;
			case 'save_daily_summary':
				$result = 0;
				$res = $db->insert( 'ddf_summary', array(
					'sum_type'        => 'total',
					'sum_key'       => 'listings',
					'sum_value'     => $data['total']['listings'],
					'sum_date'      => $data['cron']['sum_date']
				), array( '%s', '%s', '%s', '%s' ) );
				$result += ($res) ? 0 : 1;
				$res = $db->insert( 'ddf_summary', array(
					'sum_type'        => 'total',
					'sum_key'       => 'paid_agents',
					'sum_value'     => $data['total']['paid_agents'],
					'sum_date'      => $data['cron']['sum_date']
				), array( '%s', '%s', '%s', '%s' ) );
				$result += ($res) ? 0 : 1;
				$res = $db->insert( 'ddf_summary', array(
					'sum_type'        => 'total',
					'sum_key'       => 'agent_listings',
					'sum_value'     => $data['total']['agent_listings'][0]['total'],
					'sum_date'      => $data['cron']['sum_date']
				), array( '%s', '%s', '%s', '%s' ) );
				$result += ($res) ? 0 : 1;
				$res = $db->insert( 'ddf_summary', array(
					'sum_type'        => 'total',
					'sum_key'       => 'agent_sold',
					'sum_value'     => $data['total']['agent_sold'][0]['total'],
					'sum_date'      => $data['cron']['sum_date']
				), array( '%s', '%s', '%s', '%s' ) );
				$result += ($res) ? 0 : 1;
				break;
			case 'get_sold_listing':
				$result = $db->get_results( "SELECT `PropertyID` FROM `ddf_listing` WHERE `PaidAgent` ='NO' AND `Sold`='YES' AND `PropertyID` = ".(int)$data['PropertyID']." AND DATE(`LastUpdated`) <= '".$data['date']."'", ARRAY_A );
				#debug_log( $action.'.log', "SELECT `PropertyID` FROM `ddf_listing` WHERE `PaidAgent` ='NO' AND `Sold`='YES' AND `PropertyID` = ".(int)$data['PropertyID']." AND DATE(`LastUpdated`) <= '".$data['date']."'");

				break;
			case 'remove_sold_listing':
				$result = $db->delete( 'ddf_listing', array( 'PropertyID' => (int)$data['PropertyID'] ), array( '%d' ) );
				$result = $db->delete( 'ddf_listing_meta', array( 'PropertyID' => (int)$data['PropertyID'] ), array( '%d' ) );
				#debug_log('remove.log', $data);

				break;
			case 'fetch_incomplete_listing':
				#debug_log('step.log', '2');
				$sql = "SELECT `PropertyID` AS 'id' FROM `ddf_listing` WHERE (`OfficeID` = '' OR `PostalCode` = '' OR `City` = '') AND `Sold`='No' ";
				$result = $db->get_results( $sql, ARRAY_A );
				#debug_log('sql.log',$sql);
				#debug_log('sql.log',$result);
				break;
			case 'get_cron_sum':
				$sql = "SELECT * FROM `ddf_summary` WHERE `sum_date` >= '".$data['date']."' order by `sum_date`";
				$result = $db->get_results( $sql, ARRAY_A );
				#debug_log('sql.log', $sql);
				break;
			case 'scode_by_office':
				$result = $db->get_results("SELECT COUNT(*) as 'total', `OfficeID`, LOWER(`OfficeNAME`) AS 'OfficeNAME' FROM `ddf_listing` WHERE `OfficeNAME` != '' GROUP BY `OfficeID` ORDER BY `OfficeNAME` ASC");
				break;
			case 'scode_by_office':
				$result = $db->get_results("SELECT COUNT(*) as 'total', `OfficeID`, LOWER(`OfficeNAME`) AS 'OfficeNAME' FROM `ddf_listing` WHERE `OfficeNAME` != '' GROUP BY `OfficeID` ORDER BY `OfficeNAME` ASC");
				break;
			case 'scode_by_city':
				$result = $db->get_results("SELECT count(*) as 'total', LCASE(`City`) AS 'City' FROM `ddf_listing` WHERE `City` != '' AND `SOLD` = 'NO'  GROUP BY `City` HAVING count(*) > 100 ORDER BY `total` DESC", ARRAY_A);
				break;
			case 'scode_by_ptype':
				$result = $db->get_results("SELECT COUNT(*) AS `total`, `PropertyType` FROM `ddf_listing` WHERE `PropertyType` != '' AND `SOLD` = 'NO'  GROUP BY `PropertyType`", ARRAY_A);
				break;
			case 'scode_by_btype':
				$result = $db->get_results("SELECT COUNT(*) AS `total`, `BuildingType` FROM `ddf_listing` WHERE `BuildingType` != '' AND `SOLD` = 'NO' GROUP BY `BuildingType` ", ARRAY_A);
				break;
		}

		return $result;
	}

	public function get_agent_listings($agent_id){

		$results = $this->my_query('get_agent_listings', array('agent_id'=>$agent_id) );

		if($results){
			return json_encode($results);
		}else{
			return false;
		}
	}

	/**
	 * @ToDo Change Database
	 * @return bool|string]
	 */
	public function get_agents(){

		$results = $this->my_query('get_agents', array(), false );

		if($results){
			return json_encode($results);
		}else{
			return false;
		}

	}

	/**
	 * @ToDo Change Database
	 * @return bool|string'
	 */
	public function get_agents_details(){

		$results = $this->my_query('get_agents_details', array(), false );

		if($results){
			return json_encode($results);
		}else{
			return false;
		}

	}
	public function update_sold_listing($listingID, $agent_sold = false, $task_id=''){

		try{

			if($agent_sold == false){
				//@ToDo May need delete the listing not belongs to paid agent
				$this->remove_sold_listing( $listingID );
			}

			$this->my_query( 'sold_listing', array('PropertyID'=>(int)$listingID, 'TaskID'=>$task_id) );
			$result = 1;

		}catch (Exception $e){
			debug_log('err.log',$e->getMessage());
			$result = 0;
		}

		return $result;
	}

	public function mark_paid_agent($listingID){

		try{

			$this->my_query( 'mark_paid_agent', array('PropertyID'=>(int)$listingID) );

			$result = 1;

		}catch (Exception $e){
			debug_log('err.log',$e->getMessage());
			$result = 0;
		}

		return $result;
	}

	public function get_total_listings(){

		$listingIDs = $this->my_query( 'get_total_listings' );

		if($listingIDs){
			return json_encode($listingIDs);
		}else {
			return 0;
		}
	}

	public function get_active_listings(){
		$listingIDs = $this->my_query( 'get_active_listings' );

		if($listingIDs){
			return json_encode($listingIDs);
		}else {
			return 0;
		}
	}
	public function get_group_listings( $offset =0 ){

		$listingIDs = $this->my_query( 'get_group_listings', array('offset' => $offset) );

		if($listingIDs){
			return json_encode($listingIDs);
		}else {
			return 0;
		}
	}

	public function get_daily_summary(){
		$error = 0;
		$result = array();

		# Get Total Listings
		$result['total']['listings'] = $this->my_query( 'get_total_listings_counts' );


		# Get Paid Agent Total
		# @ToDo Change Database

		$paid_agents = $this->my_query( 'get_agents', array(), false );

		$result['total']['paid_agents'] = count( $paid_agents );
		debug_log( 'result.log', $result );
		# Get Paid Agent Total
		$result['cron']['sum_date'] = date( 'Y-m-d', strtotime('Yesterday') );

		# Get Agents Listing Status
		$result['total']['agent_listings'] = $this->my_query( 'get_paid_agent_listings' );
		$result['total']['agent_sold'] = $this->my_query( 'get_paid_agent_sold_listings' );

		$res = $this->my_query( 'save_daily_summary', $result );

		return $res;
	}
	public function remove_sold_listing( $PropertyID='' ){

		if(empty($PropertyID))
			return;

		$sold_listing = $this->my_query(

			'get_sold_listing', array(
				'PropertyID' => $PropertyID,
				'date'=>date('Y-m-d', strtotime('-4 days'))
				)
			);

		if($sold_listing){
			$sold_listing = current($sold_listing);
			$dir = dirname( __FILE__ ).'/photos/'.$sold_listing['PropertyID'];

			if( is_dir( $dir ) ){

				$files = array_diff(scandir($dir), array('.','..'));

				foreach( $files as $k=>$img){
						unlink($dir.'/'.$img);
				}
				rmdir($dir);
			}

			$sold_listing = $this->my_query( 'remove_sold_listing', array('PropertyID'=>$sold_listing['PropertyID']) );
		}
	}

	public function fetch_incomplete_listing(){
		#debug_log('step.log', '2');
		$listings = $this->my_query( 'fetch_incomplete_listing' );
		#debug_log('step.log', '3');
		return json_encode($listings);
	}
	public function get_cron_sum( $date ){
		if(empty($date)){
			$date  = '-10 days';
		}

		$report = $this->my_query(
			'get_cron_sum',
			array( 'date'=>date('Y-m-d', strtotime($date)) )
		);

		return json_encode($report);
	}

	public function get_scode_by_office(){

		$agents = $this->my_query( 'scode_by_office' );

		if($agents) {
			return json_encode( $agents );
		}else{
			return 0;
		}
	}
	public function get_scode_by_city(){

		$result = $this->my_query( 'scode_by_city' );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}
	public function get_scode_by_ptype(){

		$result = $this->my_query( 'scode_by_ptype' );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}
	public function get_scode_by_btype(){

		$result = $this->my_query( 'scode_by_btype' );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}
}


function debug_log($file_name, $content) {

	global $debug;

	if($debug) {
		$log_file    = dirname( __FILE__ ) . '/log/' . $file_name;
		file_put_contents( $log_file, print_r( $content, 1 ), FILE_APPEND);
	}
}



if( $debug == false and $_POST['token'] != '+T4179^818:C3Vp9+8%3+^=9||=2kJ') {
	exit(1);
}

try {

	debug_log('post.log', $_POST);
	$dba = new DB_Action();

	if( isset($_POST['action']) ){

		switch ($_POST['action']){
			case 'sold':
				$result = $dba->update_sold_listing( $_POST['PropertyID'], false,  $_POST['task_id'] );
				break;

			case 'agent_sold':
				$result = $dba->update_sold_listing( $_POST['PropertyID'], true,  $_POST['task_id']);
				break;

			case 'paid_agents':
				$result = $dba->get_agents();
				break;
			case 'paid_agent_details':
				$result = $dba->get_agents_details();
				break;
			case 'mark_paid':
				$result = $dba->mark_paid_agent( $_POST['PropertyID'] );
				break;

			case 'get_agent_listings':
				$result = $dba->get_agent_listings( $_POST['agent_id'] );
				break;
			case 'fetch_listings_total':
				$result = $dba->get_total_listings();
				break;

			case 'fetch_active_listings':
				$result = $dba->get_active_listings();
				break;

			case 'get_group':
				$result = $dba->get_group_listings( $_POST['offset'] );
				break;

			case 'daily_summary':
				$result = $dba->get_daily_summary();
				break;
			case 'remove_sold_test':
				$result = $dba->remove_sold_listing( $_POST['PropertyID'] );
				break;
			case 'fetch_incomplete':
				#debug_log('step.log', '1');
				$result = $dba->fetch_incomplete_listing();
				break;
			case 'cron_report':
				$param = isset( $_POST['day'] ) ? $_POST['day'] : 10;
				$result = $dba->get_cron_sum( $param );
				break;
			case 'sc_by_office':
				$result = $dba->get_scode_by_office();
				break;
			case 'sc_by_city':
				$result = $dba->get_scode_by_city();
				break;
			case 'sc_by_ptype':
				$result = $dba->get_scode_by_ptype();
				break;
			case 'sc_by_btype':
				$result = $dba->get_scode_by_btype();
				break;
		}

	}

	echo $result;

}catch (Exception $e){
	file_put_contents( dirname( __FILE__ ) . '/data_exception_'.date('Y-m-d').'.log', $e->getMessage(), FILE_APPEND );
	exit( $e->getMessage() );

}