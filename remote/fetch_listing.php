<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-08-03
 * Time: 11:42 AM
 */

require_once('../wp-config.php');
$debug = true;

class DB_Action{

	private $db_name;
	private $db_user;
	private $db_pass;
	private $db_host;

	function __construct(){
		# Staging
		#$this->db_name = 'snapshot_realtechpro';
		#$this->db_user = 'realtechpro';
		#$this->db_pass = 'GYzvK7b87kflC6o79bxa';
		#$this->db_host = '127.0.0.1';
		# Live
		$this->db_name = 'wp_realtechpro';
		$this->db_user = 'realtechpro';
		$this->db_pass = 'GYzvK7b87kflC6o79bxa';
		$this->db_host = '127.0.0.1';
	}

	private function my_query($action, $data=array(), $custom_db=false){

		global $wpdb;

		$db = $wpdb;
		$result = 0;
		$extra_where = '';
		if($custom_db){
			$db = new wpdb( $this->db_user, $this->db_pass, $this->db_name, $this->db_host );
		}

		if(isset($data['list_type'])){
			if( strtolower($data['list_type']) == 'sale')
				$extra_where = " AND  `ListingType` = 'For sale'";
			else
				$extra_where = " AND  `ListingType` LIKE '%" . $data['list_type'] . "%' ";
		}
		debug_log('query_'.$action.'.log', print_r($data,1) ."\n\n");
		switch ( $action ){

			case 'sql_all':

				$select = "SELECT * FROM `ddf_listing`";
				$where  = " WHERE `Sold` = 'No'";
				if( isset($data['data'])){
					$keyword = idx($data['data'],'keyword',false);
					if( $keyword ){
						$where .= " AND ( "
						          ." `Address` LIKE '%".$keyword."%' "
						          ." OR `PostalCode` LIKE '".$keyword."%' "
						          ." OR `City` LIKE '%".$keyword."%' "
						          ." OR `ListingID` LIKE '%".$keyword."%' "
						          .")";
					}

					$keyword = idx($data['data'], 'PropertyType', false);
					if( $keyword ){
						if($keyword == 'Residential'){
							$where .= " AND  `PropertyType` IN ('Single Family', 'Multi-family', 'Condo/Strata')";
						}else {
							$where .= " AND  `PropertyType` = '" . $keyword . "' ";
						}
					}

					$keyword = idx($data['data'],'BuildingType',false);
					if( $keyword ){
						$where .= " AND  `BuildingType` = '".$keyword."' ";
					}

					if(isset($data['listing_type'])){
						$where .= $extra_where;
					}else {
						$keyword = idx( $data['data'], 'TransactionType', false );
						if ( $keyword ) {
							$where .= " AND  `ListingType` = '" . $keyword . "' ";
						}
					}


					$keyword = idx($data['data'],'BedRange',false);
					if( $keyword ){
						$where .= " AND  `BedroomsTotal` >= ".$keyword;
					}

					$keyword = idx($data['data'],'BathRange',false);
					if( $keyword ){
						$where .= " AND  `BathroomTotal` >= ".$keyword;
					}

					$keyword = idx($data['data'], 'PriceMin', false);
					if( $keyword ){
						$where .= " AND CONVERT(SUBSTRING_INDEX(`Price`, '/', 1),UNSIGNED INTEGER) >= ".$keyword;
					}

					$keyword = idx($data['data'], 'PriceMax',false);
					if( $keyword ){
						$where .= " AND  CONVERT(SUBSTRING_INDEX(`Price`, '/', 1),UNSIGNED INTEGER) <= ".$keyword;
					}

					$keyword = idx($data['data'], 'OpenHouse', false);
					if( $keyword ){
						$where .= " AND  `OpenHouse` > 0";
					}

					$keyword = idx($data['data'], 'StartDate',false);
					if( $keyword ){
						$where .= " AND  `LastUpdated` > '".$keyword."'";
					}

					$keyword = idx($data['data'], 'city',false);
					if( $keyword ){
						$where .= " AND LCASE(`City`) = '".$keyword."'";
					}
				}

				$order  = " ORDER BY `LastUpdated` DESC ";
				$limit  = " LIMIT ".$data['limit']." OFFSET ".$data['offset'];

				$sql = $select . $where . $order . $limit;
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_single':
				$sql = "SELECT * FROM `ddf_listing` WHERE `PropertyID` = ".$data['pid'];
				$res['data'] = $db->get_results( $sql, ARRAY_A);


				$sql = "SELECT * FROM `ddf_listing_meta` WHERE `PropertyID` = ".$data['pid'];
				$res['meta'] = $db->get_results( $sql, ARRAY_A);
				$result = $res;
				break;
			case 'sql_agent_ddf_listings':


				$sql = "SELECT * FROM `ddf_listing` WHERE `PaidAgent` = 'YES' ".$extra_where." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC ";

				if( $data['limit'] > 0){
					$sql .= " LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				}
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_agent_past_listings':

				$sql = "SELECT * FROM `ddf_listing` WHERE `PaidAgent` = 'YES' ".$extra_where." AND `Sold` = 'YES' ORDER BY `LastUpdated` DESC ";

				if( $data['limit'] > 0){
					$sql .= " LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				}
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_selected_agent_listings':

				$where = $limit = '';
				if( isset($data['type']) and $data['type']=='active' ){
					$where = " AND `Sold` = 'NO'";
				}

				if( isset($data['type']) and $data['type']=='past' ){
					$where = " AND `Sold` = 'YES'";
				}

				if( isset($data['limit'])  ){
					$limit = " LIMIT ".$data['limit'];
				}


				$sql = "SELECT * FROM `ddf_listing` WHERE `AgentID` '%".$data['AgentID']."%' " . $where . $extra_where." ORDER BY `LastUpdated` DESC ".$limit;
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_office_listings':
				$city = '';
				if( isset($data['city'])  ){
					$city = " AND LCASE(`City`) = '".$data['city']."'";
				}
				$sql = "SELECT * FROM `ddf_listing` WHERE `OfficeID` IN ('".str_replace(',', "','",$data['OfficeID'])."') ".$city." ".$extra_where." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_city_listings':
				$sql = "SELECT * FROM `ddf_listing` WHERE LCASE(`City`) = '".$data['City']."' ".$extra_where." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_selected_postalcode_listings':
				$sql = "SELECT * FROM `ddf_listing` WHERE `PostalCode` IN ('".str_replace(',', "','", $data['PostalCode'])."') ".$extra_where." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_selected_property_id':
				$sql = "SELECT * FROM `ddf_listing` WHERE `PropertyID` IN ('".str_replace(',', "','", $data['PropertyID'])."')";
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_selected_property_type':
				$city = '';
				if( isset($data['city'])  ){
					$city = " AND LCASE(`City`) = '".$data['city']."'";
				}
				$sql = "SELECT * FROM `ddf_listing` WHERE `PropertyType` IN ('".str_replace(',', "','", $data['PropertyType'])."') ".$city.$extra_where." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_listing_type':
				$city = '';
				if( isset($data['city'])  ){
					$city = " AND LCASE(`City`) = '".$data['city']."'";
				}
				$sql = "SELECT * FROM `ddf_listing` WHERE `ListingType` = '".$data['ListingType']."' ".$city." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_building_type':
				$city = '';
				if( isset($data['city'])  ){
					$city = " AND LCASE(`City`) = '".$data['city']."'";
				}
				$sql = "SELECT * FROM `ddf_listing` WHERE `BuildingType` IN ('".str_replace(',', "','", $data['BuildingType'])."') ".$city.$extra_where." AND `Sold` = 'NO' ORDER BY `LastUpdated` DESC  LIMIT ".$data['limit']." OFFSET ".$data['offset'];
				$result = $db->get_results( $sql, ARRAY_A);
				break;
			case 'sql_selected_price':
				$min_where = '';
				$max_where = '';
				$sub_where = '';
				$city = '';

				$sql = "SELECT * FROM `ddf_listing`";
				$where = 0;
				$order  = " ORDER BY `LastUpdated` DESC ";
				$limit  = " LIMIT ".$data['limit']." OFFSET ".$data['offset'];

				if( isset($data['min']) ){
					$min_where = " CONVERT(SUBSTRING_INDEX(`Price`, '/', 1),UNSIGNED INTEGER) >= ".$data['min'];
					$where++;
				}

				if( isset($data['max']) ){
					$max_where = " CONVERT(SUBSTRING_INDEX(`Price`, '/', 1),UNSIGNED INTEGER) >= ".$data['max'];
					$where++;
				}
				if( isset($data['status'])){
					if( $data['status'] == 'sale' ){
						$sub_where = ' AND `ListingType` = "For sale" ';
					}elseif($data['status'] == 'other' ){
						$sub_where = ' AND `ListingType` != "For sale" ';
					}
				}

				if( isset($data['city'])  ){
					$city = " AND LCASE(`City`) = '".$data['city']."'";
				}
				if( $where > 0 ){

					if( $where == 2 ){
						$sql = $sql . ' WHERE `Sold` = "NO" AND ' . $min_where . ' AND ' . $max_where . $sub_where . $city .$order . $limit;
					}else {
						$sql = $sql . ' WHERE `Sold` = "NO" AND ' . $min_where . $max_where . $sub_where . $city . $order . $limit;
					}
				}
				$result = $db->get_results( $sql, ARRAY_A);
				break;
		}
		debug_log( $action.'.log', $sql ."\n\n");
		return $result;
	}

	public function dba_fn_single($data){

		$result = $this->my_query( 'sql_single', array( 'pid' => $data['pid'] ) );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_common($data, $fn){
/*
		$param = array( 'limit' => $data['limit'], 'offset'=>$data['offset'] );
		if( isset($data['data'])){
			$param['data'] = $data['data'];
		}
		if( isset($data['city'])){
			$param['city'] = $data['city'];
		}
*/
		$result = $this->my_query( str_replace('fn', 'sql', $fn), $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_selected_agent_listings($data){

		$result = $this->my_query( 'sql_selected_agent_listings', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}
	public function dba_fn_office_listings( $data ){
		$result = $this->my_query( 'sql_office_listings', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}
	public function dba_fn_city_listings($data){
		$result = $this->my_query( 'sql_city_listings', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_selected_postalcode_listings($data){
		$result = $this->my_query( 'sql_selected_postalcode_listings', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_selected_property_id( $data ){

		$result = $this->my_query( 'sql_selected_property_id', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_selected_property_type( $data ){

		$result = $this->my_query( 'sql_selected_property_type', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_listing_type( $data ){
		debug_log('param.log', $data);
		$result = $this->my_query( 'sql_listing_type', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_building_type( $data ){
		debug_log('param.log', $data);
		$result = $this->my_query( 'sql_building_type', $data );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}

	public function dba_fn_selected_price( $param  ){

		$result = $this->my_query( 'sql_selected_price', $param );

		if($result) {
			return json_encode( $result );
		}else{
			return 0;
		}
	}
}


function debug_log($file_name, $content) {

	global $debug;

	if($debug) {
		$log_file    = dirname( __FILE__ ) . '/log/' . $file_name;
		file_put_contents( $log_file, print_r( $content, 1 ), FILE_APPEND);
	}
}
function std_to_array( $std ){
	return json_decode( json_encode($std), true );
}
function idx($array, $key, $default = null) {

	if( !isset($array) ){
		return $default;
	}

	if( is_object($array)){
		$array = (array)$array;
	}

	return array_key_exists($key, $array) ? $array[$key] : $default;
}

if( $debug == false and !in_array($_POST['token'], array('+T4179^818:C3Vp9+8%3+^=9||=2kJ', 'Vp9+8%3+^=9||=2kJ+T4179^818:C3')) ){
	exit(1);
}

try {

	debug_log('post_'.$_POST['action'].'.log', $_POST);
	$dba = new DB_Action();

	if( isset($_POST['action']) ){

		$limit  = isset( $_POST['limit'] )  ? $_POST['limit']   : '30';
		$offset = isset( $_POST['offset'] ) ? $_POST['offset']  : '0';
		$query  = isset( $_POST['data'] )   ? $_POST['data']    : false;
		$list_type  = isset( $_POST['list_type'] ) ? $_POST['list_type'] : false;
		$param  = array( 'limit'=>$limit, 'offset' => $offset);

		if($query){
			$param['data'] = std_to_array( json_decode( urldecode( $query )));
		}
		# list_type  Rent or Sale
		if( isset($param['data']['meta']) and array_key_exists("list_type",$param['data']['meta']) ){

			debug_log( 'list_type_1.0_'.$_POST['action'].'.log', $param );
			$param['list_type'] = $param['data']['meta']['list_type'];
			debug_log( 'list_type_1.1_'.$_POST['action'].'.log', $param );
		}
		if($list_type){
			$param['list_type'] = $list_type;
		}

		switch ($_POST['action']){

			case 'fn_single':
				$result = $dba->dba_fn_single( array( 'pid'=>$_POST['ListingID']) );
				break;

			case 'fn_all':
			case 'fn_agent_ddf_listings':
			case 'fn_agent_past_listings':

				if($_POST['action'] =='fn_all' and isset($_POST['city'])){
					$param['data']['city'] = $_POST['city'];
				}
				$result = $dba->dba_fn_common( $param, $_POST['action'] );
				break;
			case 'fn_selected_agent_listings':

				$param['AgentID'] = $_POST['AgentKey'];

				if( isset($_POST['type'])){
					$param['type'] = $_POST['type'];
				}
				if( isset($_POST['limit'])){
					$param['limit'] = $_POST['limit'];
				}
				if( isset($_POST['city'])){
					$param['city'] = $_POST['city'];
				}
				$result = $dba->dba_fn_selected_agent_listings( $param  );
				break;
			case 'fn_office_listings':

				// Load for shortcode
				if( isset($_POST['office_id'])){
					$param['OfficeID']=$_POST['office_id'];
				}

				if( isset($_POST['city'])){
					$param['city'] = $_POST['city'];
				}

				// Load for view more button
				if( isset($param['data']) ) {

					if( isset( $param['data']['meta']['meta'] ) ) {
						$param['OfficeID'] = $param['data']['meta']['meta'];
					}

					if( isset( $param['data']['meta']['city'] ) ){
						$param['city'] = $param['data']['meta']['city'];
					}
				}
				$result = $dba->dba_fn_office_listings( $param  );

				break;

			case 'fn_city_listings':

				if( isset($param['data']) ) {
					$param['City'] = $param['data']['meta']['meta'];
				}else{
					$param['City'] = $_POST['city'];
				}

				$result = $dba->dba_fn_city_listings( $param  );
				break;

			case 'fn_selected_postalcode_listings':

				if( isset($param['data']) and  array_key_exists("meta",$param['data']['meta']) ) {
					$param['PostalCode'] = $param['data']['meta']['meta'];
				}else{
					$param['PostalCode'] = $_POST['postal_code'];
				}
				if( strpos(',', $_POST['PostalCode'] ) !== false) {
					$ids = explode( ',', $_POST['PostalCode'] );
					foreach ( $ids as $k => $v ) {
						$temp[] = trim( $v );
					}
				}
				if($temp) {
					$param['PostalCode'] = implode( ',', $temp );
				}
				$result = $dba->dba_fn_selected_postalcode_listings( $param  );

				break;

			case 'fn_selected_property_id':

				$ids = explode(',',$_POST['property_id']);
				foreach($ids as $k=>$v){
					$temp[]=trim($v);
				}
				$param['PropertyID'] = implode(',',$temp);

				$result = $dba->dba_fn_selected_property_id( $param  );
				break;

			case 'fn_selected_property_type':

				if( isset($param['data'])) {
					$param['PropertyType'] = $param['data']['meta']['meta'];
					if(isset( $param['data']['meta']['city'])){
						$param['city'] = $param['data']['meta']['city'];
					}
				}else{
					$param['PropertyType'] = $_POST['property_type'];
				}

				if( isset($_POST['city'])){
					$param['city'] = $_POST['city'];
				}

				$result = $dba->dba_fn_selected_property_type( $param  );
				break;

			case 'fn_listing_type':

				if( isset($param['data'])) {
					$param['ListingType'] = $param['data']['meta']['meta'];
					if(isset( $param['data']['meta']['city'])){
						$param['city'] = $param['data']['meta']['city'];
					}
				}else{
					$param['ListingType'] = $_POST['listing_type'];
				}

				if( isset($_POST['city'])){
					$param['city'] = $_POST['city'];
				}

				$result = $dba->dba_fn_listing_type( $param  );
				break;

			case 'fn_selected_building_type':

				if( isset($param['data'])) {
					$param['BuildingType'] = $param['data']['meta']['meta'];
					if(isset( $param['data']['meta']['city'])){
						$param['city'] = $param['data']['meta']['city'];
					}
				}else{
					$param['BuildingType'] = $_POST['building_type'];
				}
				
				if( isset($_POST['city'])){
					$param['city'] = $_POST['city'];
				}

				$result = $dba->dba_fn_building_type( $param  );
				break;
			case 'fn_selected_price':
				if( isset($param['data'])) {
					if ( isset( $param['data']['meta']['max'] ) ) {
						$param['max'] = $param['data']['meta']['max'];
					}
					if ( isset( $param['data']['meta']['min'] ) ) {
						$param['min'] = $param['data']['meta']['min'];
					}
					if ( isset( $param['data']['meta']['status'] ) ) {
						$param['status'] = $param['data']['meta']['status'];
					}
					if ( isset( $param['data']['meta']['city'] ) ) {
						$param['city'] = $param['data']['meta']['city'];
					}
				}else {
					if ( isset( $_POST['max'] ) ) {
						$param['max'] = $_POST['max'];
					}
					if ( isset( $_POST['min'] ) ) {
						$param['min'] = $_POST['min'];
					}
					if ( isset( $_POST['status'] ) ) {
						$param['status'] = $_POST['status'];
					}
					if( isset($_POST['city'])){
						$param['city'] = $_POST['city'];
					}
				}
				$result = $dba->dba_fn_selected_price( $param  );
				break;
		}

	}
	debug_log('param_'.$_POST['action'].'.log', $param);
	echo $result;

}catch (Exception $e){
	file_put_contents( dirname( __FILE__ ) . '/data_exception_'.date('Y-m-d').'.log', $e->getMessage(), FILE_APPEND );
	exit( $e->getMessage() );

}