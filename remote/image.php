<?php
/**
 * Created by PhpStorm.
 * User: tonylee
 * Date: 2015-08-03
 * Time: 11:42 AM
 */

require_once('../wp-config.php');

function save_data(){
	global $wpdb;

	$listingID  = $_POST['listing_id'];
	$image_name = $_POST['name'];
	$image_data = $_POST['image'];
	$result = 0;

	$wpdb->flush();
	$counter = $wpdb->get_var( "SELECT COUNT(*) FROM `ddf_listing_meta` WHERE `meta_id`='{$image_name}' AND `PropertyID`={$listingID}");

	if($counter > 0 ){

		$wpdb->update( 'ddf_listing_meta',
			array(
				'meta_value' => $image_data,
			),
			array(
				'PropertyID' => $listingID,
				'meta_key'   => 'photos',
				'meta_id'    => $image_name,
			),
			array( '%s'),
			array( '%d', '%s', '%s')
		);

	}else {

		$wpdb->insert( 'ddf_listing_meta', array(
			'PropertyID'    => $listingID,
			'meta_key'      => 'photos',
			'meta_id'       => $image_name,
			'meta_value'    => $image_data,

		), array( '%d', '%s', '%s', '%s' ) );
	}

	if($wpdb->last_error) {
		$result = 1;
	}
	return $result;
}
function makeDir($path)
{
	return is_dir($path) || mkdir($path);
}
function save_image(){

	$server_path = dirname(__FILE__);
	$image_folder = $server_path . '/photos/'.$_POST['listing_id'];
	$image = $image_folder.'/'.$_POST['name'];
	$image_data = base64_decode($_POST['image']);

	try{
		makeDir( $image_folder );
		file_put_contents( $image,  $image_data );
	}catch (Exception $e){
		file_put_contents( dirname( __FILE__ ). '/'.date('Y-m-d').'_error.log',  $e->getMessage(), FILE_APPEND, $image_data );
		return '1';
	}

	return 0;

}

if( empty($_POST['listing_id']) or empty($_POST['image']) or $_POST['token'] != '+T4179^818:C3Vp9+8%3+^=9||=2kJ') {
	exit(1);
}

try {

	if(file_exists(dirname(__FILE__) . '/photos/'.$_POST['listing_id'].'/'.$_POST['name'])){
		echo 0;
	}else {
		echo save_image();
	}
}catch (Exception $e){
	exit( $e->getMessage() );
}