<?php
/**
 * Social Brand Amplifier - Worker
 */
$workers = array();

# Project Token #
$workers['iw_token']      = 'Hc_bEGrJf2wpPJAzXkQkBKV3BBY';
$workers['iw_project_id'] = '553ead7f09159d000600010c';

# IRON #
$workers['iron'] = new IronWorker(array(
	'token'		=> $workers['iw_token'],
	'project_id'=> $workers['iw_project_id']
));

$workers['RETS'] = array(
	'username' => "sQ5oAmL0idpecN2MtWqDaTq0",
	'password' => "dUxnQoPo7Z4KLTOa5iK0hRsT",
	'url'      => "http://data.crea.ca/Login.svc/Login",
	'time'     => "-48 hours",
	'limit'    => 5,
	'size'     => "LargePhoto",
	'image_url'=> "http://realtechpro.io/_ddf_handler/image.php",
	'data_url' => "http://realtechpro.io/_ddf_handler/data.php",
	'listing_url' => "http://realtechpro.io/_ddf_handler/fetch_all.php"
);

# SBA Worker MAP #
$GLOBALS['worker_map'] = array(

	// -------------------------------------------------------------------------------------
	// Fetch from CREA
	// -------------------------------------------------------------------------------------
	#"CRON_Get_Master_Listings"	=> "CRON-Get-Master-Listings",

	#"W_Fetch_Master_Listings"   => "W-Fetch-Master-Listings-1",
	#"W_Fetch_Master_Listings"   => "W-Fetch-Master-Listings-2",

	#"W_Get_Listing_Photos"		    => "W-Get-Listing-Photos-0",
	#"W_Get_Listing_Photos"		    => "W-Get-Listing-Photos-1",
	#"W_Get_Listing_Photos"		    => "W-Get-Listing-Photos-2",
	#"W_Get_Listing_Photos"		    => "W-Get-Listing-Photos-3",
	#"W_Get_Listing_Photos"		    => "W-Get-Listing-Photos-4",

	// -------------------------------------------------------------------------------------
	// Fetch from WPEngine
	// -------------------------------------------------------------------------------------

	#"CRON_GET_DDF_Listings"	  => "CRON-GET-DDF-Listings",
	#"W_Fetch_DDF_Listings"       => "W-Fetch-DDF-Listings-0",
	#"W_Fetch_DDF_Listings"       => "W-Fetch-DDF-Listings-1",
	#"W_Fetch_DDF_Listings"       => "W-Fetch-DDF-Listings-2",
	#"W_Fetch_DDF_Listings"       => "W-Fetch-DDF-Listings-3",
	#"W_Fetch_DDF_Listings"       => "W-Fetch-DDF-Listings-4",


	// -------------------------------------------------------------------------------------
	// Check Paid Agent
	// -------------------------------------------------------------------------------------
	#'CRON_GET_Agents_Listings'  => 'CRON-GET-Agents-Listings',
	#'W_Fetch_Agent_Listings'    => 'W-Fetch-Agent-Listings-1',
	#'W_Fetch_Agent_Listings'    => 'W-Fetch-Agent-Listings-2',

	#'CRON_Daily_Summary'        => 'CRON-Daily-Summary'

	// -------------------------------------------------------------------------------------
	// Fix Incomplete Listings
	// -------------------------------------------------------------------------------------
	#'W_Update_Listings'          => 'W-Update-Listings'
	#'W_Check_Sold_DDF_Listings'   => 'W-Check-Sold-DDF-Listings'
);

if( count($GLOBALS['worker_map']) < 1){
	exit('No Worker to upload.');
}