<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;
	private $worker;

	private $ListingsURI;
	private $RETS_LimitPerQuery;
	private $totalAvailable;


	function __construct(){

		# Get all default arguments
		$this->args = getArgs();
		$this->payload = $this->std_to_array($this->args['payload']);

		$this->worker = new IronWorker(array(
			'token'		=> $this->args['config']['iw_token'],
			'project_id'=> $this->args['config']['iw_project_id']
		));

		$this->ListingsURI = $this->args['config']['ListingsURI'];

	}

	public function run(){

		try{

			# Get all the paid agent ids
			$PaidAgentIDs = $this->post_data(array('action'=>'daily_summary'),$this->ListingsURI);

			if( $PaidAgentIDs != 0 ) {
				$this->kill_error('Failed to process daily summary on WPEngine.');
			}

		}catch( Exception $e ){
            $this->kill_error( 'Error:  ' . $e->getMessage() );
        }
	}

	private function post_data( $data, $uri='' ) {

		$curl = curl_init( $uri );
		$data["token"] = "+T4179^818:C3Vp9+8%3+^=9||=2kJ";
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30 );

		$response = curl_exec( $curl );

		curl_close( $curl );

		return $response;
	}

	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}

// Run worker
$worker = new Worker();
$worker->run();

?>
