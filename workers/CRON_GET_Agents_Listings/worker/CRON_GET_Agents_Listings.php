<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;
	private $worker;

	private $ListingsURI;
	private $RETS_LimitPerQuery;
	private $totalAvailable;


	function __construct(){

		# Get all default arguments
		$this->args = getArgs();
		$this->payload = $this->std_to_array($this->args['payload']);

		$this->worker = new IronWorker(array(
			'token'		=> $this->args['config']['iw_token'],
			'project_id'=> $this->args['config']['iw_project_id']
		));

		$this->RETS_LimitPerQuery = $this->args['config']['LimitPerQuery'];
		$this->ListingsURI = $this->args['config']['ListingsURI'];

		# Set Total Number of Querying listing
		if( isset($this->payload['query_total'])) {
			$this->totalAvailable = $this->payload['query_total'];
		}

	}


	public function run(){

		try{

			# Get all the paid agent ids
			$PaidAgentIDs = $this->post_data(array('action'=>'paid_agents'),$this->ListingsURI);

			if( strlen($PaidAgentIDs) > 1 ) {

				$PaidAgentIDs = $this->std_to_array( json_decode( $PaidAgentIDs ) );
				$this->do_log( 'Total agent: ' .count($PaidAgentIDs) );

				foreach($PaidAgentIDs as $k=>$agent_id){

					$worker = ( ( $k%2 == 0 ) ? '-2':'-1' );
					# assign the agent id to each worker
					$this->worker->postTask(
						'W-Fetch-Agent-Listings'.$worker,
						array(
							'agent_id' => $agent_id['meta_value']
						),
						array(  'priority'  => 0 )
					);

				}

				$this->do_log('done');

			}else{
				$this->kill_error('Failed to fetch agent id from WPEngine');
			}

		}catch( Exception $e ){
            $this->kill_error( 'Error:  ' . $e->getMessage() );
        }
	}

	private function post_data( $data, $uri='' ) {

		$curl = curl_init( $uri );
		$data["token"] = "+T4179^818:C3Vp9+8%3+^=9||=2kJ";
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30 );

		$response = curl_exec( $curl );

		curl_close( $curl );

		return $response;
	}

	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}

// Run worker
$worker = new Worker();
$worker->run();

?>
