<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;
	private $worker;

	private $TimeBackPull;
	private $RETS;
	private $RETSURL;
	private $RETSUsername;
	private $RETSPassword;
	private $RETS_PhotoSize;
	private $RETS_LimitPerQuery;
	private $totalAvailable;


	function __construct(){

		# Get all default arguments
		$this->args = getArgs();

		# Get all pass in payload
		$this->payload = $this->std_to_array($this->args['payload']);

		$this->worker = new IronWorker(array(
			'token'		=> $this->args['config']['iw_token'],
			'project_id'=> $this->args['config']['iw_project_id']
		));

		# Assign default variable
		$this->RETSUsername = $this->args['config']['RETSUsername'];
		$this->RETSPassword = $this->args['config']['RETSPassword'];
		$this->RETSURL      = $this->args['config']['RETSURL'];
		$this->RETS_LimitPerQuery = $this->args['config']['LimitPerQuery'];
		$this->RETS_PhotoSize     = $this->args['config']['PhotoSize'];

		# Initial the since date and reassign if needed
		$this->TimeBackPull = $this->args['config']['TimeBackPull'];
		if(isset($this->payload['since'])){
			$this->TimeBackPull = $this->payload['since'];
		}

		# Set Query Limit
		$this->RETS_LimitQuery = 0;
		if(isset($this->payload->limit)){
			$this->RETS_LimitQuery = $this->payload['limit'];
		}

		# Set Total Number of Querying listing
		if( isset($this->payload['query_total'])) {
			$this->totalAvailable = $this->payload['query_total'];
		}
	}


	public function run(){

		if( !isset($this->RETSURL) or !isset($this->RETSUsername) or !isset($this->RETSPassword)){
			$this->kill_error('No initial data assigned.');
			return;
		}

		try{
			# Initial Connection
			$this->RETS = new PHRets();
			$this->RETS->Connect($this->RETSURL, $this->RETSUsername, $this->RETSPassword);
			$this->RETS->AddHeader("RETS-Version", "RETS/1.7.2");
			$this->RETS->AddHeader('Accept', '/');
			$this->RETS->SetParam('compression_enabled', true);
			
			# Initial Query Parameter
			$this->DBML = "(LastUpdated=" . date('Y-m-d', strtotime($this->TimeBackPull)) . ")";
			$params = array(
				"Limit" => 1,
				"Format" => "STANDARD-XML",
				"Count" => 1
			);

			# Get and validate the query result
			$results = $this->RETS->SearchQuery("Property", "Property", $this->DBML, $params);
			$totalAvailable = $results["Count"];
			if(empty($totalAvailable) || $totalAvailable == 0) {
				$this->do_log('No available listing. TimeBackPull: ' . $this->TimeBackPull );
				$this->kill_error( print_r( $this->RETS->GetLastServerResponse(), true ) );
				return;
			}
			$this->do_log('totalAvailable real: ' .  $totalAvailable );

			# Set the custom loop limit if possible
			if( isset($this->totalAvailable) ){
				$totalAvailable = $this->totalAvailable;
			}

			# Logging information
			$this->do_log('totalAvailable: ' .  $totalAvailable );
			#$this->do_log('LimitPerQuery: ' .  $this->RETS_LimitPerQuery);
			#$this->do_log('Loop before: ' .  $totalAvailable / $this->RETS_LimitPerQuery);
			$this->do_log('Total Loops: ' .  ceil($totalAvailable / $this->RETS_LimitPerQuery));

			# Start loop query result
			for($i = 0; $i < ceil($totalAvailable / $this->RETS_LimitPerQuery); $i++) {

				$worker = ( ( $i%2 == 0 ) ? '-2':'-1' );
				$this->worker->postTask(
					'W-Fetch-Master-Listings'.$worker,
					array(
						'offset' => ( $i * $this->RETS_LimitPerQuery )
					),
					array(  'priority'  => 0 )
				);

			}

			$this->RETS->Disconnect();
			$this->do_log( "Done" );
			
		}catch( Exception $e ){
            $this->kill_error( 'Error:  ' . $e->getMessage() );
        }
	}

	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}

// Run worker
$worker = new Worker();
$worker->run();

?>
