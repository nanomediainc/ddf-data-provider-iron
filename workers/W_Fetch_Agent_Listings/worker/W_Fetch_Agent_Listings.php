<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;

	private $RETS;
	private $RETSURL;
	private $RETSUsername;
	private $RETSPassword;

	private $DataURI;
	private $ListingsURI;


	function __construct(){

		# Get default variables
		$this->args = getArgs();
		$this->ListingsURI = $this->args['config']['ListingsURI'];

		$this->payload = $this->std_to_array($this->args['payload']);

		# Assign the default custom variables
		$this->RETSUsername = $this->args['config']['RETSUsername'];
		$this->RETSPassword = $this->args['config']['RETSPassword'];
		$this->RETSURL      = $this->args['config']['RETSURL'];

	}


	public function run(){

		try{
			# Get agent's listings
			$param = array('agent_id'=>$this->payload['agent_id'], 'action'=>'get_agent_listings');
			$listingIDs = $this->post_data($param, $this->ListingsURI);

			if( strlen($listingIDs) > 1 ) {

				$listingIDs = $this->std_to_array( json_decode( $listingIDs ) );
				$this->check_sold_mark_paid( $listingIDs );
				$this->do_log('done');
			}

		}catch( Exception $e ){

			$this->kill_error( 'Error:  ' . $e->getMessage() );

		}

	}

	private function check_sold_mark_paid( $listingIDs ){

		# Connect to CREA
		$this->RETS = new PHRets();
		$this->RETS->Connect($this->RETSURL, $this->RETSUsername, $this->RETSPassword);
		$this->RETS->AddHeader("RETS-Version", "RETS/1.7.2");
		$this->RETS->AddHeader('Accept', '/');
		$this->RETS->SetParam('compression_enabled', true);

		foreach($listingIDs as $k=>$listing){

			# Mark as Paid Agent
			$this->post_data(
				array(
					'PropertyID'=> $listing['PropertyID'],
					'action'    => 'mark_paid'
				),
				$this->ListingsURI
			);
			$this->do_log( $listing['PropertyID']  . ' is Paid Agent');

			# --------------------------------------------------------------------------------

			# Fetch listing from CREA to check sold listing
			$results = $this->RETS->SearchQuery(
				"Property",
				"Property",
				"(ID={$listing['PropertyID']})",
				array( "Format" => "STANDARD-XML", "Count" => 1, "Culture"=>"en-CA", "Limit"=>1)
			);

			# Mark as Sold Property if applicable
			if( current( $results['Count'] ) < 1 ){

				$this->post_data(array(
					'PropertyID'=> $listing['PropertyID'],
					'action'    => 'agent_sold',
					'task_id'   => $this->args['task_id']
					),
					$this->ListingsURI
				);

				$this->do_log( $listing['PropertyID']  . ' Sold');
			}
		}
	}

	private function post_data( $data=array(), $uri='' ) {

		$curl = curl_init( $uri );
		$data["token"] = "+T4179^818:C3Vp9+8%3+^=9||=2kJ";
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30 );

		$response = curl_exec( $curl );
		curl_close( $curl );

		return $response;
	}

	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}
	public function test(){
		$this->do_log($this->args);
		$this->do_log($this->payload);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}
// Run worker
$worker = new Worker();
$worker->run();

?>
