<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;

	private $RETS;
	private $RETSURL;
	private $RETSUsername;
	private $RETSPassword;

	private $DataURI;
	private $ListingsURI;


	function __construct(){

		# Get default variables
		$this->args = getArgs();
		$this->ListingsURI = $this->args['config']['ListingsURI'];
		$this->DataURI = $this->args['config']['DataURI'];

		$this->payload = $this->std_to_array($this->args['payload']);

		# Assign the default custom variables
		$this->RETSUsername = $this->args['config']['RETSUsername'];
		$this->RETSPassword = $this->args['config']['RETSPassword'];
		$this->RETSURL      = $this->args['config']['RETSURL'];

	}


	public function run(){

		try{
			$this->check_single_sold( $this->payload['ListingID'] );

		}catch( Exception $e ){

			$this->kill_error( 'Error:  ' . $e->getMessage() );

		}

	}

	private function check_single_sold( $listingID ){

		# Connect to CREA
		$this->RETS = new PHRets();
		$this->RETS->Connect($this->RETSURL, $this->RETSUsername, $this->RETSPassword);
		$this->RETS->AddHeader("RETS-Version", "RETS/1.7.2");
		$this->RETS->AddHeader('Accept', '/');
		$this->RETS->SetParam('compression_enabled', true);

		$results = $this->RETS->SearchQuery(
			"Property",
			"Property",
			"(ID={$listingID})",
			array( "Format" => "STANDARD-XML", "Count" => 1, "Culture"=>"en-CA", "Limit"=>1)
		);

		#Property Sold
		if($results) {
			$results = $this->std_to_array($results);
			$this->do_log( current($results['Count'])  );
			if ( current($results['Count']) < 1 ) {
				$this->update_sold_listing( $listingID );
			}
		}

	}

	private function update_sold_listing( $listing ){

		$param = array('PropertyID' => $listing, 'action'=>'sold', 'task_id'=>$this->args['task_id']);
		$this->fetch_data($param, $this->ListingsURI );

	}

	private function fetch_data( $data=array(), $uri='' ) {

		$curl = curl_init( $uri );
		$data["token"] = "+T4179^818:C3Vp9+8%3+^=9||=2kJ";
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30 );

		$response = curl_exec( $curl );
		curl_close( $curl );

		return $response;
	}


	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}

	public function test(){
		$this->do_log($this->args);
		$this->do_log($this->payload);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}
// Run worker
$worker = new Worker();
$worker->run();

?>
