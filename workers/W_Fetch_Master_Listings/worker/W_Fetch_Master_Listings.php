<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;
	private $worker;

	private $debugMode;
	private $TimeBackPull;
	private $RETS;
	private $RETSURL;
	private $RETSUsername;
	private $RETSPassword;
	private $RETS_PhotoSize;
	private $RETS_LimitPerQuery;
	private $startOffset;
	private $totalAvailable;


	function __construct(){

		# Get all default arguments
		$this->args = getArgs();

		# Get all pass in payload
		$this->payload = $this->std_to_array($this->args['payload']);

		if( isset($this->payload['offset'])) {
			$this->startOffset = $this->payload['offset'];
		}else{
			$this->do_log('No offset.');
			$this->kill_error( print_r($this->payload, true));
		}

		$this->worker = new IronWorker(array(
			'token'		=> $this->args['config']['iw_token'],
			'project_id'=> $this->args['config']['iw_project_id']
		));

		# Assign default variable
		$this->RETSUsername = $this->args['config']['RETSUsername'];
		$this->RETSPassword = $this->args['config']['RETSPassword'];
		$this->RETSURL      = $this->args['config']['RETSURL'];
		$this->TimeBackPull = $this->args['config']['TimeBackPull'];
		$this->RETS_LimitPerQuery = $this->args['config']['LimitPerQuery'];
		$this->RETS_PhotoSize     = $this->args['config']['PhotoSize'];

		# Get the since data if possible
		if(isset($this->payload['since'])){
			$this->TimeBackPull = $this->payload['since'];
		}

	}

	public function run(){

		if( !isset($this->RETSURL) or !isset($this->RETSUsername) or !isset($this->RETSPassword)){
			$this->kill_error('No initial data assigned.');
			return;
		}

		try{

			# Connect to CREA
			$this->RETS = new PHRets();
			$this->RETS->Connect($this->RETSURL, $this->RETSUsername, $this->RETSPassword);
			$this->RETS->AddHeader("RETS-Version", "RETS/1.7.2");
			$this->RETS->AddHeader('Accept', '/');
			$this->RETS->SetParam('compression_enabled', true);

			# Set Query Parameters
			$this->DBML = "(LastUpdated=" . date('Y-m-d', strtotime($this->TimeBackPull)) . ")";
			$params = array(
				"Limit" => $this->RETS_LimitPerQuery,
				"Format" => "STANDARD-XML",
				"Count" => 1,
				"Offset" => $this->startOffset
			);

			# Query result
			$results = $this->RETS->SearchQuery("Property", "Property", $this->DBML, $params);
			if( !isset($results["Properties"]) or empty($results["Properties"])){
				$this->kill_error('No listing available');
			}

			foreach($results["Properties"] as $index => $listing){

				$this->worker->postTask(
					'W-Get-Listing-Photos-'.$index,
					array(
						'listingID' => $listing["@attributes"]["ID"]
						//'agentID'   => $AgentId
					),
					array( 'priority' => 1 )
				);

			}

			$this->RETS->Disconnect();
			$this->do_log( "Done" );

		}catch( Exception $e ){
			$this->kill_error( 'Error:  ' . $e->getMessage() );
		}
	}

	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}

// Run worker
$worker = new Worker();
$worker->run();
?>
