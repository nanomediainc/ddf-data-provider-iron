<?php
defined('DS') OR define('DS', '../../');

require_once "phar://" . DS . "iron_worker.phar";
require_once DS. "vars.php";
global $workers;

$result = $workers['iron']->upload(
	dirname(__FILE__)."/worker/",
	basename(__DIR__).".php",
	$GLOBALS['worker_map'][basename(__DIR__)],
	array(
		'max_concurrency' => 10,
		'stack' => 'php-5.4',
		'config' => json_encode(
			array(
				'iw_token'		=> $workers['iw_token'],
				'iw_project_id'	=> $workers['iw_project_id'],
				'RETSUsername'  => $workers['RETS']['username'],
				'RETSPassword'  => $workers['RETS']['password'],
				'RETSURL'       => $workers['RETS']['url'],
				'TimeBackPull'  => $workers['RETS']['time'],
				'LimitPerQuery' => $workers['RETS']['limit'],
				'PhotoSize'     => $workers['RETS']['size'],
				'DataURI'       => $workers['RETS']['data_url'],
				'ImageURI'       => $workers['RETS']['image_url'],
			)
		)
	)
);

echo basename(__DIR__).' - uploaded <br>';
