<?php
require_once dirname(__FILE__) . "/lib/DDF_lib/PHRets_CREA.php";
require_once dirname(__FILE__) . "/lib/MySQL_lib/MysqliDb.php";
require_once "phar://" . dirname(__FILE__)  ."/lib/IronWorker/iron_worker.phar";

class Worker{

	private $args;
	private $payload;
	private $worker;

	private $TimeBackPull;
	private $RETS;
	private $RETSURL;
	private $RETSUsername;
	private $RETSPassword;
	private $RETS_PhotoSize;
	private $RETS_LimitPerQuery;
	private $DataURI;
	private $ImageURI;
	private $type;
	private $listing;


	function __construct(){

		# Get default variables
		$this->args = getArgs();

		$this->payload = $this->std_to_array($this->args['payload']);

		$this->worker = new IronWorker(array(
			'token'		=> $this->args['config']['iw_token'],
			'project_id'=> $this->args['config']['iw_project_id']
		));

		# Assign the default custom variables
		$this->RETSUsername = $this->args['config']['RETSUsername'];
		$this->RETSPassword = $this->args['config']['RETSPassword'];
		$this->RETSURL      = $this->args['config']['RETSURL'];
		$this->DataURI      = $this->args['config']['DataURI'];
		$this->ImageURI     = $this->args['config']['ImageURI'];
		$this->RETS_LimitPerQuery   = $this->args['config']['LimitPerQuery'];
		$this->RETS_PhotoSize       = $this->args['config']['PhotoSize'];


		$this->TimeBackPull = $this->args['config']['TimeBackPull'];
		if(isset($this->payload['since'])){
			$this->TimeBackPull = $this->payload['since'];
		}

		$this->RETS_LimitQuery = 0;
		if(isset($this->payload->limit)){
			$this->RETS_LimitQuery = $this->payload['limit'];
		}

		$this->type = array(
			'image'     => false,
			'listing'   => false
		);

		if( isset($this->payload['type']) ){
			$this->type[ $this->payload['type'] ] = true;
		}else{
			$this->type = array(
				'image'     => true,
				'listing'   => true
			);
		}

		$this->startOffset = 0;
		$this->listing['ID']        = $this->payload['listingID'];
		$this->listing['img_err']   = 0;
		$this->listing['lis_err']   = 0;
	}


	public function run(){

		try{
			$this->do_log($this->type);
			# Connect to CREA
			$this->RETS = new PHRets();
			$this->RETS->Connect($this->RETSURL, $this->RETSUsername, $this->RETSPassword);
			$this->RETS->AddHeader("RETS-Version", "RETS/1.7.2");
			$this->RETS->AddHeader('Accept', '/');
			$this->RETS->SetParam('compression_enabled', true);

			# ---------------------------------------------------
			# Fetch and post the listing data to WP Engine
			# ---------------------------------------------------
			if( $this->type['listing'] === true ) {

				$this->listing['lis_err'] = $this->post_listing_to_wp(
					array(
						'listingID' => $this->listing['ID']
					),
					$this->DataURI
				);
			}

			# ---------------------------------------------------
			# Fetch and post the listing photos data to WP Engine
			# ---------------------------------------------------
			if(  $this->type['image'] === true and $this->listing['lis_err'] == 0 ) {

				$this->post_photo_to_wp(
						array(
								'listingID' => $this->listing['ID']
							 ),
							 $this->ImageURI
						);
			}

			$this->RETS->Disconnect();
			$this->do_log('done');

		}catch( Exception $e ){

			$this->kill_error( 'Error:  ' . $e->getMessage() );

		}

	}
	function post_data( $data=array(), $uri='' ) {

		$curl = curl_init( $uri );
		$data["token"] = "+T4179^818:C3Vp9+8%3+^=9||=2kJ";
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30 );

		$response = curl_exec( $curl );
		curl_close( $curl );

		return $response;
	}
	function post_listing_to_wp($listingInfo, $uri){

		$results = $this->RETS->SearchQuery(
			"Property",
			"Property",
			"(ID={$listingInfo['listingID']})",
			array( "Format" => "STANDARD-XML", "Count" => 1, "Culture"=>"en-CA", "Limit"=>1)
		);

		if( current( $results['Count'] ) < 1 ){

			$this->do_log('No Property Found.');
			return 1;
		}
		$property = current($results['Properties']);

		# Only allow fetching the following listings
		$allow = array( "Ontario", "British Columbia" );
		if( !in_array( $property['Address']['Province'], $allow )  ){
			return 1;
		}

		$property = $this->get_long_lat($property);
		$property['task_id'] = $this->args['task_id'];

		$payload = array("data"=> urlencode(json_encode($property)) );

		// '0' means data saved. '1' means data not saved
		$this->listing['lis_err'] = $this->post_data($payload, $uri);

		if( trim($this->listing['lis_err']) != 0){
			$this->kill_error('Failed to save listing data to WPEngine end.');
			return;
		}else{
			$this->do_log( "1. Listing Sent :" . $this->listing['ID'] );
			return 0;
		}



	}
	function get_long_lat($property){

		if(isset($property['Address'])) {
			$address = urlencode( implode( '+', $property['Address'] ) );
			$url     = "http://maps.google.com/maps/api/geocode/json?address={$address}&sensor=false&region=" . $property['Address']['Country'];

			$json    = file_get_contents( $url );
			$listing = $this->std_to_array( json_decode( urldecode( $json ) ) );
			if ( isset( $listing['results'] ) ) {
				$location                         = current( $listing['results'] );
				$property['Address']['lat']       = $location['geometry']['location']['lat'];
				$property['Address']['lng']       = $location['geometry']['location']['lng'];
				$property['Address']['formatted'] = $location['formatted_address'];
				$this->do_log( 'Lat and Long is fetched' );
			}
		}

		return $property;
	}
	function post_photo_to_wp($listingInfo, $uri)
	{

		$listingID = $listingInfo['listingID'];
		$photos = $this->RETS->GetObject("Property", $this->RETS_PhotoSize, $listingID, '*');

		if(!$photos or !is_array($photos))
		{
			$this->do_log( "Cannot Locate Photos" );
			return;
		}

		foreach($photos as $photo)
		{
			if(
				(!isset($photo['Content-ID']) || !isset($photo['Object-ID']))
				||
				(is_null($photo['Content-ID']) || is_null($photo['Object-ID']))
				||
				($photo['Content-ID'] == 'null' || $photo['Object-ID'] == 'null')
			)
			{
				continue;
			}

			$listing     = $photo['Content-ID'];
			$number      = $photo['Object-ID'];
			$destination = $listingID."_".$number.".jpg";
			$photoData   = $photo['Data'];

			$photo_data  = array(
				'image'         => base64_encode($photoData),
				'name'          => $destination,
				'listing_id'    => $listing
			);

			$result = $this->post_data( $photo_data, $uri );
			if( $result != 0 ){ // '0' means image saved. '1' means image not saved

				$this->do_log( $destination.' | failed to saved on WPEngine end.' );
				$this->listing['img_err']++;
			}

		}

		if( $this->listing['img_err'] == 0 ) {
			$this->do_log( "2. Photos Sent: " . $this->listing['ID'] );
			return 0;
		}else{
			return;
		}

	}

	private function do_log( $log_txt = '' ){
		echo (is_string($log_txt)  ?  $log_txt : print_r($log_txt, TRUE) );
		echo "\r\n";
	}

	private function kill_error( $log_txt = ''){
		$this->do_log( $log_txt );
		exit(1);
	}
	public function test(){
		$this->do_log($this->args);
		$this->do_log($this->payload);
	}

	private function std_to_array( $std ){
		return json_decode( json_encode($std), true );
	}
}
// Run worker
$worker = new Worker();
$worker->run();

?>
